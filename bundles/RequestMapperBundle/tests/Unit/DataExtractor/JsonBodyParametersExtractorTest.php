<?php

namespace RequestMapperBundle\Tests\Unit\DataExtractor;

use PHPUnit\Framework\TestCase;
use RequestMapperBundle\DataExtractor\JsonBodyParametersExtractor;
use Symfony\Component\HttpFoundation\Request;

class JsonBodyParametersExtractorTest extends TestCase
{
    public function testGetGroups(): void
    {
        $extractor = new JsonBodyParametersExtractor();

        $this->assertContains('json', $extractor->getGroups());
    }

    public function testExtract(): void
    {
        $id = 100;
        $name = 'John';
        $surname = 'Doe';

        $extractor = new JsonBodyParametersExtractor();
        $request = new Request(
            ['id' => $id],
            [],
            [],
            [],
            [],
            [],
            json_encode(['name' => $name, 'surname' => $surname])
        );

        $result = $extractor->extract($request);

        $this->assertArrayNotHasKey('id', $result);
        $this->assertArrayHasKey('name', $result);
        $this->assertArrayHasKey('surname', $result);
        $this->assertEquals($name, $result['name']);
        $this->assertEquals($surname, $result['surname']);
    }
}
