<?php

namespace RequestMapperBundle\Tests\Unit\DataExtractor;

use PHPUnit\Framework\TestCase;
use RequestMapperBundle\DataExtractor\GetParametersExtractor;
use Symfony\Component\HttpFoundation\Request;

class GetParametersExtractorTest extends TestCase
{
    public function testGetGroups(): void
    {
        $extractor = new GetParametersExtractor();

        $this->assertContains('get', $extractor->getGroups());
    }

    public function testExtract(): void
    {
        $id = 100;
        $name = 'John';
        $surname = 'Doe';

        $extractor = new GetParametersExtractor();
        $request = new Request(['id' => $id], ['name' => $name, 'surname' => $surname]);

        $result = $extractor->extract($request);

        $this->assertArrayHasKey('id', $result);
        $this->assertEquals($id, $result['id']);
        $this->assertArrayNotHasKey('name', $result);
        $this->assertArrayNotHasKey('surname', $result);
    }
}
