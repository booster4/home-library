<?php

namespace RequestMapperBundle\Normalizer;

interface ArrayDenormalizerInterface
{
    /**
     * @param array<string, mixed> $data
     * @param string $class
     * @param string[] $groups
     * @param object $object
     * @return object
     */
    public function denormalize(array $data, string $class, array $groups, object $object): object;
}
