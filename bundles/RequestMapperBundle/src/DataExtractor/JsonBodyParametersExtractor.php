<?php

namespace RequestMapperBundle\DataExtractor;

use JsonException;
use Symfony\Component\HttpFoundation\Request;

class JsonBodyParametersExtractor implements DataExtractorInterface
{
    public function extract(Request $request): array
    {
        try {
            return json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException) { // todo
            return [];
        }
    }

    public function getGroups(): array
    {
        return ['json'];
    }
}
