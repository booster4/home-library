<?php

namespace RequestMapperBundle\DataExtractor;

use Symfony\Component\HttpFoundation\Request;

class PathParametersExtractor implements DataExtractorInterface
{
    public function extract(Request $request): array
    {
        return $request->attributes->get('_route_params') ?? [];
    }

    public function getGroups(): array
    {
        return ['path'];
    }
}
