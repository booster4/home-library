<?php

namespace RequestMapperBundle\DataExtractor;

use Symfony\Component\HttpFoundation\Request;

interface DataExtractorInterface
{
    /**
     * @return array<string, mixed>
     */
    public function extract(Request $request): array;

    /**
     * @return string[]
     */
    public function getGroups(): array;
}
