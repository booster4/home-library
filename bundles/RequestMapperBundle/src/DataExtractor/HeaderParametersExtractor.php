<?php

declare(strict_types=1);

namespace RequestMapperBundle\DataExtractor;

use Symfony\Component\HttpFoundation\Request;

class HeaderParametersExtractor implements DataExtractorInterface
{
    /**
     * @psalm-suppress InvalidReturnType
     * @psalm-suppress InvalidReturnStatement
     */
    public function extract(Request $request): array
    {
        return $request->headers->all();
    }

    public function getGroups(): array
    {
        return ['header'];
    }
}
