<?php

namespace RequestMapperBundle;

use RequestMapperBundle\DataExtractor\DataExtractorInterface;
use RequestMapperBundle\DTO\MappableRequestInterface;
use RequestMapperBundle\Normalizer\ArrayDenormalizer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

// todo test me
final readonly class RequestValueResolver implements ArgumentValueResolverInterface
{
    /**
     * @param DataExtractorInterface[] $dataExtractor
     */
    public function __construct(
        private ArrayDenormalizer $denormalizer,
        private array $dataExtractor,
    ) {
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return in_array(MappableRequestInterface::class, class_implements((string) $argument->getType()));
    }

    /**
     * @return iterable<object>
     * @throws ExceptionInterface
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $dto = new ((string) $argument->getType());

        foreach ($this->dataExtractor as $extractor) {
            $this->denormalizer->denormalize(
                $extractor->extract($request),
                (string) $argument->getType(),
                $extractor->getGroups(),
                $dto,
            );
        }

        return [$dto];
    }
}
