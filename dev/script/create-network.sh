#!/usr/bin/env bash

echo "Run create-network.sh"

networkName="$1"
networkMask="$2"

if [[ "$(docker network ls -q -f name="${networkName}")" ]]; then
  echo "Network ${networkName} at ${networkMask} exists"
else
  echo "Create network:"
  docker network create "${networkName}" --subnet "${networkMask}.0/16" --gateway "${networkMask}.1"
fi
