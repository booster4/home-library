<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Default\File;
use App\Repository\Exceptions\FileNotFoundException;
use App\Repository\Filter\ListFilter;
use App\Service\Walker\DTO\FileDTOInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

class FileRepository extends ServiceEntityRepository implements FileRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, File::class);
    }

    public function getByID(Uuid $id): File
    {
        $file = $this->find($id);

        if (null === $file) {
            throw FileNotFoundException::notFoundByID($id);
        }

        return $file;
    }

    public function getByHash(FileDTOInterface $fileDTO): File
    {
        $file = $this->findOneBy([
            'hashMethod' => $fileDTO->getHashMethod(),
            'hash' => $fileDTO->getHash(),
        ]);

        if (null === $file) {
            throw FileNotFoundException::notFoundByHash($fileDTO);
        }

        return $file;
    }

    public function getList(ListFilter $filter): array
    {
        $qb = $this
            ->createQueryBuilder('f')
            ->leftJoin('f.book', 'b');

        if (false === $filter->hasBook()) {
            $qb->where($qb->expr()->isNull('b.id'));
        } elseif (true === $filter->hasBook()) {
            $qb->where($qb->expr()->isNotNull('b.id'));
        }
        if (null !== $filter->getOffset()) {
            $qb->setFirstResult($filter->getOffset());
        }
        if (null !== $filter->getLimit()) {
            $qb->setMaxResults($filter->getLimit());
        }

        $qb->orderBy(
            new OrderBy(
                'f.' . $filter->getSortingField()->value,
                $filter->getSortingDirection()->value
            ),
        );

        return $qb->getQuery()->execute();
    }
}
