<?php

namespace App\Repository\Filter;

enum SortingField: string
{
    case CREATED_AT = 'createdAt';
}
