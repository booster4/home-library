<?php

declare(strict_types=1);

namespace App\Repository\Filter;

use App\Enum\SortingDirection;

class ListFilter
{
    private ?bool $hasBook = null;

    private ?int $limit = null;

    private ?int $offset = null;

    private SortingField $sortingField = SortingField::CREATED_AT;

    private SortingDirection $sortingDirection = SortingDirection::ASC;

    public function hasBook(): ?bool
    {
        return $this->hasBook;
    }

    public function setHasBook(?bool $hasBook): self
    {
        $this->hasBook = $hasBook;

        return $this;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    public function getOffset(): ?int
    {
        return $this->offset;
    }

    public function setOffset(int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }

    public function getSortingField(): SortingField
    {
        return $this->sortingField;
    }

    public function setSortingField(SortingField $sortingField): self
    {
        $this->sortingField = $sortingField;

        return $this;
    }

    public function getSortingDirection(): SortingDirection
    {
        return $this->sortingDirection;
    }

    public function setSortingDirection(SortingDirection $sortingDirection): self
    {
        $this->sortingDirection = $sortingDirection;

        return $this;
    }
}
