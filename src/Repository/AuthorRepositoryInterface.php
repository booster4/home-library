<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Default\Author;
use App\Repository\Exceptions\BookAuthorNotFoundException;

interface AuthorRepositoryInterface
{
    /**
     * @throws BookAuthorNotFoundException
     */
    public function getByName(string $name): Author;
}
