<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Default\File;
use App\Repository\Exceptions\FileNotFoundException;
use App\Repository\Filter\ListFilter;
use App\Service\Walker\DTO\FileDTOInterface;
use Exception;
use Symfony\Component\Uid\Uuid;

interface FileRepositoryInterface
{
    /**
     * @throws Exception
     * @throws FileNotFoundException
     */
    public function getByHash(FileDTOInterface $fileDTO): File;

    /**
     * @throws Exception
     * @throws FileNotFoundException
     */
    public function getByID(Uuid $id): File;

    /**
     * @return File[]
     */
    public function getList(ListFilter $filter): array;
}
