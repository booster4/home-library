<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Default\Author;
use App\Repository\Exceptions\BookAuthorNotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AuthorRepository extends ServiceEntityRepository implements AuthorRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Author::class);
    }

    public function getByName(string $name): Author
    {
        $author = $this->findOneBy(['name' => $name]);

        if (null === $author) {
            throw BookAuthorNotFoundException::notFoundByName($name);
        }

        return $author;
    }
}
