<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Default\CommandRun;
use App\Repository\Exceptions\CommandRunNotFoundByRunIDException;
use Symfony\Component\Uid\AbstractUid;

interface CommandRunRepositoryInterface
{
    /**
     * @throws CommandRunNotFoundByRunIDException
     */
    public function getByRunID(AbstractUid $id): CommandRun;
}
