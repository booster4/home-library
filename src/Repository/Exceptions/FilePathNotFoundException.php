<?php

declare(strict_types=1);

namespace App\Repository\Exceptions;

use Exception;
use Symfony\Component\Uid\AbstractUid;

final class FilePathNotFoundException extends Exception
{
    private const TEMPLATE = "File path ID %s not found";

    public static function build(AbstractUid $id): FilePathNotFoundException
    {
        return new self(self::provideMessage($id));
    }

    private static function provideMessage(AbstractUid $id): string
    {
        return sprintf(self::TEMPLATE, $id->toRfc4122());
    }
}
