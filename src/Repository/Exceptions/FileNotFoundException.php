<?php

declare(strict_types=1);

namespace App\Repository\Exceptions;

use App\Service\Walker\DTO\FileDTOInterface;
use Exception;
use Symfony\Component\Uid\Uuid;

final class FileNotFoundException extends Exception
{
    public static function notFoundByHash(FileDTOInterface $fileDTO): FileNotFoundException
    {
        return new self(
            sprintf(
                "File %s with hash %s by method %s not found",
                $fileDTO->getPath(),
                $fileDTO->getHash() ?: '',
                $fileDTO->getHashMethod()?->value ?: '',
            )
        );
    }

    public static function notFoundByID(Uuid $uuid): FileNotFoundException
    {
        return new self(
            sprintf(
                "File with ID %s not found",
                $uuid->toRfc4122(),
            )
        );
    }
}
