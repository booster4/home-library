<?php

declare(strict_types=1);

namespace App\Repository\Exceptions;

use App\Service\Walker\DTO\FileDTOInterface;
use Exception;
use Symfony\Component\Uid\Uuid;

final class BookAuthorNotFoundException extends Exception
{
    public static function notFoundByName(string $name): BookAuthorNotFoundException
    {
        return new self(
            sprintf(
                "Book author \"%s\" not found",
                $name,
            )
        );
    }
}
