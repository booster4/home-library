<?php

declare(strict_types=1);

namespace App\Repository\Exceptions;

use Exception;
use Symfony\Component\Uid\AbstractUid;

class CommandRunNotFoundByRunIDException extends Exception
{
    private const TEMPLATE = 'CommandRun not found by runID %s';

    public static function build(AbstractUid $id): self
    {
        return new self(sprintf(self::TEMPLATE, $id->toRfc4122()));
    }
}
