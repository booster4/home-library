<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Default\File\Path;
use App\Repository\Exceptions\FilePathNotFoundException;
use Symfony\Component\Uid\AbstractUid;

interface FilePathRepositoryInterface
{
    /**
     * @throws FilePathNotFoundException
     */
    public function getByID(AbstractUid $id): Path;
}
