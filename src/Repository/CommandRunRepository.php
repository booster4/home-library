<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Default\CommandRun;
use App\Repository\Exceptions\CommandRunNotFoundByRunIDException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\AbstractUid;

class CommandRunRepository extends ServiceEntityRepository implements CommandRunRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CommandRun::class);
    }

    public function getByRunID(AbstractUid $id): CommandRun
    {
        $commandRun = $this->findOneBy(['runID' => $id]);

        if (null === $commandRun) {
            throw CommandRunNotFoundByRunIDException::build($id);
        }

        return $commandRun;
    }
}
