<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Default\File\Path;
use App\Repository\Exceptions\FilePathNotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\AbstractUid;

class FilePathRepository extends ServiceEntityRepository implements FilePathRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Path::class);
    }

    public function getByID(AbstractUid $id): Path
    {
        $path = $this->find($id);

        if (null === $path) {
            throw FilePathNotFoundException::build($id);
        }

        return $path;
    }
}
