<?php

declare(strict_types=1);

namespace App\Enum\CommandRun;

enum Status: string
{
    case RUN = 'run';
    case ERROR = 'error';
    case COMPLETE = 'complete';
}
