<?php

declare(strict_types=1);

namespace App\Enum\Book;

enum Language: string
{
    case RUS = 'rus';
    case ENG = 'eng';
}
