<?php

namespace App\Enum\Book;

enum Status: string
{
    case ACTUAL = 'actual';

    case DELETE = 'delete';
}
