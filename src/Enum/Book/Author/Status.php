<?php

namespace App\Enum\Book\Author;

enum Status: string
{
    case ACTUAL = 'actual';

    case DELETE = 'delete';
}
