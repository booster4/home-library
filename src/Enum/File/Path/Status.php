<?php

declare(strict_types=1);

namespace App\Enum\File\Path;

enum Status: string
{
    case ACTIVE = 'active';
    case MISSED = 'missed';
    case DELETE = 'delete';
}
