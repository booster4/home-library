<?php

declare(strict_types=1);

namespace App\Enum\File;

enum FileHashMethod: string
{
    case BODY_MD5 = 'file_body_md5';
    case BODY_SHA = 'file_body_sha';
}
