<?php

namespace App\Enum;

enum SortingDirection: string
{
    case ASC = 'asc';
    case DESC = 'desc';
}
