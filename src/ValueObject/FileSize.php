<?php

declare(strict_types=1);

namespace App\ValueObject;

use Exception;
use Stringable;

class FileSize implements Stringable
{
    private const FLOAT_FORMAT = '%s%01.2f %s';

    private const INTEGER_FORMAT = '%s%d %s';

    private const APPROXIMATELY_EQUAL_SIGN = '≈';

    private const SIGNS = [
        \App\Enum\FileSize::B,
        \App\Enum\FileSize::KB,
        \App\Enum\FileSize::MB,
        \App\Enum\FileSize::GB,
        \App\Enum\FileSize::TB,
        \App\Enum\FileSize::PB,
        \App\Enum\FileSize::EB,
        \App\Enum\FileSize::ZB,
        \App\Enum\FileSize::YB,
    ];

    private const DELIMITER = 1024;

    public function __construct(
        private readonly int $size,
        private readonly bool $strict = true,
    ) {
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function isStrict(): bool
    {
        return $this->strict;
    }

    /**
     * @throws Exception
     */
    public function toString(): string
    {
        $i = 0;
        $s = $this->size;
        while ($s >= self::DELIMITER) {
            $s /= self::DELIMITER;
            $i += 1;
        }

        // @codeCoverageIgnoreStart
        if ($i >= count(self::SIGNS)) {
            throw new Exception('Unexpected size');
        }
        // @codeCoverageIgnoreEnd

        if (self::SIGNS[$i] === \App\Enum\FileSize::B) {
            return sprintf(self::INTEGER_FORMAT, $this->provideSign(), $s, self::SIGNS[$i]->value);
        }

        return sprintf(self::FLOAT_FORMAT, $this->provideSign(), $s, self::SIGNS[$i]->value);
    }

    private function provideSign(): string
    {
        if ($this->strict) {
            return "";
        }

        return self::APPROXIMATELY_EQUAL_SIGN;
    }

    /**
     * @throws Exception
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
