<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class BasenameExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('basename', [$this, 'basenameFilter']),
        ];
    }

    public function basenameFilter(string $value): string
    {
        return basename($value);
    }
}
