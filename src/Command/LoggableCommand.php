<?php

declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Uid\AbstractUid;

abstract class LoggableCommand extends Command
{
    protected AbstractUid $id;

    public function getID(): AbstractUid
    {
        return $this->id;
    }

    protected function setID(AbstractUid $id): void
    {
        $this->id = $id;
    }
}
