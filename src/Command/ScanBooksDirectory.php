<?php

declare(strict_types=1);

namespace App\Command;

use App\Factory\UuidFactory\UuidFactoryInterface;
use App\Service\Walker\WalkerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('home-library:scan', 'Scan library directory and grab files', ['hl:s'], false)]
class ScanBooksDirectory extends LoggableCommand
{
    public function __construct(
        private readonly WalkerInterface $walker,
        private readonly UuidFactoryInterface $uuidFactory,
    ) {
        parent::__construct();

        $this->setID($this->uuidFactory->buildV7());
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->walker->walk();

        return self::SUCCESS;
    }
}
