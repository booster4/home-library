<?php

declare(strict_types=1);

namespace App\Entity\Default;

use App\Enum\CommandRun\Status;
use App\Repository\CommandRunRepository;
use Carbon\Carbon;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Uid\Uuid;

#[Entity(repositoryClass: CommandRunRepository::class)]
#[Table(name: 'command_runs')]
class CommandRun
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', unique: true, nullable: false)]
    private int $id;

    #[Column(type: 'string', nullable: false)]
    private string $name;

    #[Column(type: 'string', nullable: false, enumType: Status::class)]
    private Status $status;

    #[Column(type: 'uuid', nullable: false)]
    private Uuid $runID;

    #[Column(type: 'datetimetz', precision: 6, nullable: false)]
    private Carbon $runAt;

    #[Column(type: 'datetimetz', precision: 6, nullable: true)]
    private Carbon $doneAt;

    public function __construct()
    {
        $this->runAt = Carbon::now();
        $this->status = Status::RUN;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRunID(): Uuid
    {
        return $this->runID;
    }

    public function setRunID(Uuid $runID): self
    {
        $this->runID = $runID;

        return $this;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getRunAt(): Carbon
    {
        return $this->runAt;
    }

    public function setRunAt(Carbon $runAt): self
    {
        $this->runAt = $runAt;

        return $this;
    }

    public function getDoneAt(): Carbon
    {
        return $this->doneAt;
    }

    public function setDoneAt(Carbon $doneAt): self
    {
        $this->doneAt = $doneAt;

        return $this;
    }
}
