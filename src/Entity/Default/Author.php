<?php

declare(strict_types=1);

namespace App\Entity\Default;

use App\Enum\Book\Author\Status;
use App\Repository\AuthorRepository;
use App\Repository\FileRepository;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\Table;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;

#[Entity(repositoryClass: AuthorRepository::class)]
#[Table(name: 'authors')]
class Author
{
    #[Id]
    #[Column(type: UuidType::NAME, unique: true, nullable: false)]
    private Uuid $id;

    #[Column(type: "string", nullable: false)]
    private string $name;

    #[Column(type: "string", nullable: false)]
    private string $transliteration;

    #[ManyToMany(targetEntity: Book::class, mappedBy: 'authors')]
    private Collection $books;

    #[Column(type: "string", enumType: Status::class)]
    private Status $status;

    #[Column(type: 'datetimetz', precision: 6, nullable: false)]
    private Carbon $createdAt;

    #[Column(type: 'datetimetz', precision: 6, nullable: true)]
    private ?Carbon $updatedAt;

    public function __construct()
    {
        $this->books = new ArrayCollection();
        $this->status = Status::ACTUAL;
        $this->createdAt = Carbon::now();
    }

    public function getID(): Uuid
    {
        return $this->id;
    }

    public function setID(Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTransliteration(): string
    {
        return $this->transliteration;
    }

    public function setTransliteration(string $transliteration): self
    {
        $this->transliteration = $transliteration;

        return $this;
    }

    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books->add($book);
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        if ($this->books->contains($book)) {
            $this->books->removeElement($book);
        }

        return $this;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): self
    {
        $this->status = $status;

        return $this;
    }


    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    public function setCreatedAt(Carbon $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?Carbon $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
