<?php

declare(strict_types=1);

namespace App\Entity\Default;

use App\Entity\Default\File\Path;
use App\Repository\FileRepository;
use App\Enum\File\FileHashMethod;
use App\Enum\File\Status;
use App\ValueObject\FileSize;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use Stringable;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;

#[Entity(repositoryClass: FileRepository::class)]
#[Table(name: 'files')]
class File implements Stringable
{
    #[Id]
    #[Column(type: UuidType::NAME, unique: true, nullable: false)]
    private Uuid $id;

    #[Column(type: 'string', unique: true, nullable: false)]
    private string $hash;

    #[Column(type: 'string', nullable: false, enumType: FileHashMethod::class)]
    private FileHashMethod $hashMethod;

    #[Column(type: 'file_size', nullable: false)]
    private FileSize $size;

    #[Column(type: 'string', nullable: false, enumType: Status::class)]
    private Status $status;

    #[Column(type: 'string', nullable: false)]
    private string $mime;

    #[OneToOne(mappedBy: 'file', targetEntity: Book::class, cascade: ['persist'])]
    private ?Book $book;

    #[OneToMany(mappedBy: 'file', targetEntity: Path::class, cascade: ['persist'])]
    private Collection $paths;

    #[Column(type: 'datetimetz', precision: 6, nullable: false)]
    private Carbon $createdAt;

    #[Column(type: 'datetimetz', precision: 6, nullable: true)]
    private ?Carbon $updatedAt;

    public function __construct()
    {
        $this->createdAt = Carbon::now();
        $this->status = Status::ACTIVE;
        $this->paths = new ArrayCollection();
        $this->book = null;
        $this->updatedAt = null;
    }

    public function __toString(): string
    {
        return $this->id->toRfc4122();
    }

    public function getID(): Uuid
    {
        return $this->id;
    }

    public function setID(Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

    public function getHashMethod(): FileHashMethod
    {
        return $this->hashMethod;
    }

    public function setHashMethod(FileHashMethod $hashMethod): self
    {
        $this->hashMethod = $hashMethod;

        return $this;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getMime(): string
    {
        return $this->mime;
    }

    public function setMime(string $mime): File
    {
        $this->mime = $mime;

        return $this;
    }

    public function getBook(): ?Book
    {
        return $this->book;
    }

    public function setBook(Book $book): self
    {
        $this->book = $book;

        return $this;
    }

    /**
     * @return Collection<integer, Path>
     */
    public function getPaths(): Collection
    {
        return $this->paths;
    }

    public function addPath(Path $path): self
    {
        if (!$this->paths->contains($path)) {
            $this->paths->add($path);
            $path->setFile($this);
        }

        return $this;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    public function setCreatedAt(Carbon $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?Carbon $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getSize(): FileSize
    {
        return $this->size;
    }

    public function setSize(FileSize $size): self
    {
        $this->size = $size;

        return $this;
    }
}
