<?php

declare(strict_types=1);

namespace App\Entity\Default;

use App\Enum\Book\Language;
use App\Enum\Book\Status;
use App\Repository\FileRepository;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;

#[Entity(repositoryClass: FileRepository::class)]
#[Table(name: 'books')]
class Book
{
    #[Id]
    #[Column(type: UuidType::NAME, unique: true, nullable: false)]
    private Uuid $id;

    #[OneToOne(mappedBy: 'book', targetEntity: File::class)]
    #[JoinColumn(name: 'file_id', referencedColumnName: 'id')]
    private File $file;

    #[Column(type: 'text')]
    private string $title;

    #[Column(type: 'string', nullable: true)]
    private ?string $isbn;

    #[Column(type: 'string', enumType: Language::class)]
    private Language $language;

    #[Column(type: "string", nullable: true)]
    private ?string $publisher;

    #[Column(type: "text", nullable: true)]
    private ?string $series;

    #[Column(type: "string", nullable: true)]
    private ?string $edition;

    #[Column(type: 'integer', nullable: true)]
    private ?int $year;

    #[Column(type: 'string', enumType: Status::class)]
    private Status $status;

    #[ManyToMany(targetEntity: Author::class, inversedBy: 'books', cascade: ['persist'])]
    #[JoinTable(name: 'book_authors')]
    private Collection $authors;

    #[Column(type: 'datetimetz', precision: 6, nullable: false)]
    private Carbon $createdAt;

    #[Column(type: 'datetimetz', precision: 6, nullable: true)]
    private ?Carbon $updatedAt;

    public function __construct()
    {
        $this->createdAt = Carbon::now();
        $this->status = Status::ACTUAL;
        $this->authors = new ArrayCollection();
    }


    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getFile(): File
    {
        return $this->file;
    }

    public function setFile(File $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getISBN(): ?string
    {
        return $this->isbn;
    }

    public function setISBN(string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }

    public function setLanguage(Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getPublisher(): ?string
    {
        return $this->publisher;
    }

    public function setPublisher(?string $publisher): self
    {
        $this->publisher = $publisher;

        return $this;
    }

    public function getSeries(): ?string
    {
        return $this->series;
    }

    public function setSeries(?string $series): self
    {
        $this->series = $series;

        return $this;
    }

    public function getEdition(): ?string
    {
        return $this->edition;
    }

    public function setEdition(?string $edition): self
    {
        $this->edition = $edition;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(?int $year): Book
    {
        $this->year = $year;

        return $this;
    }

    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function addAuthor(Author $author): self
    {
        if (!$this->authors->contains($author)) {
            $this->authors->add($author);
        }

        return $this;
    }

    public function removeAuthor(Author $author): self
    {
        if ($this->authors->contains($author)) {
            $this->authors->removeElement($author);
        }

        return $this;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    public function setCreatedAt(Carbon $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?Carbon $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
