<?php

declare(strict_types=1);

namespace App\Entity\Default\File;

use App\Entity\Default\File;
use App\Repository\FilePathRepository;
use App\Enum\File\Path\Status;
use Carbon\Carbon;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;

#[Entity(repositoryClass: FilePathRepository::class)]
#[Table(name: "file_paths")]
class Path
{
    #[Id]
    #[Column(type: UuidType::NAME, unique: true, nullable: false)]
    private Uuid $id;

    #[Column(type: "text", nullable: false)]
    private string $path;

    #[Column(type: "string", nullable: false, enumType: Status::class)]
    private Status $status;

    #[ManyToOne(targetEntity: File::class, inversedBy: 'paths')]
    #[JoinColumn(name: 'file_id', referencedColumnName: 'id', nullable: false)]
    private File $file;

    #[Column(type: "datetimetz", precision: 6, nullable: false)]
    private Carbon $createdAt;

    #[Column(type: "datetimetz", precision: 6, nullable: true)]
    private ?Carbon $updatedAt;

    public function __construct()
    {
        $this->status = Status::ACTIVE;
        $this->createdAt = new Carbon();
        $this->updatedAt = null;
    }

    public function getID(): Uuid
    {
        return $this->id;
    }

    public function setID(Uuid $id): static
    {
        $this->id = $id;

        return $this;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): static
    {
        $this->path = $path;

        return $this;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): static
    {
        $this->status = $status;

        return $this;
    }

    public function getFile(): File
    {
        return $this->file;
    }

    public function setFile(File $file): Path
    {
        $this->file = $file;

        return $this;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    public function setCreatedAt(Carbon $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?Carbon $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
