<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Default\Book;
use App\Factory\BookFactory;
use App\Repository\Exceptions\FileNotFoundException;
use App\Service\BookService\DTO\BookDTO;
use Doctrine\ORM\EntityManagerInterface;

class BookService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly BookFactory $bookFactory,
    ) {
    }

    /**
     * @throws FileNotFoundException
     */
    public function create(BookDTO $dto): Book
    {
        $book = $this->bookFactory->build(
            $dto->getFileID(),
            $dto->getTitle(),
            $dto->getISBN(),
            $dto->getLanguage(),
            $dto->getSeries(),
            $dto->getYear(),
            $dto->getPublisher(),
            $dto->getEdition(),
            $dto->getAuthors(),
        );

        $this->entityManager->persist($book);

        return $book;
    }
}
