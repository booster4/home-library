<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Default\File;
use App\Entity\Default\File\Path;
use App\Enum\File\Path\Status;
use App\Factory\FileFactory;
use App\Factory\PathFactory;
use App\Repository\Exceptions\FileNotFoundException;
use App\Repository\FileRepositoryInterface;
use App\Service\Utils\RealPathFormatterInterface;
use App\Service\Walker\DTO\FileDTO;
use App\Service\Walker\DTO\PathDTO;
use App\Service\Walker\FileChecker\FSChecker;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Process;

class FileService
{
    public function __construct(
        private readonly FileRepositoryInterface $fileRepository,
        private readonly FileFactory $fileBuilder,
        private readonly PathFactory $pathBuilder,
        private readonly FSChecker $fsChecker,
        private readonly RealPathFormatterInterface $realPathFormatter,
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger,
    ) {
    }

    /**
     * @throws Exception
     */
    public function save(FileDTO $fileDTO): void
    {
        try {
            $file = $this->fileRepository->getByHash($fileDTO);
            $this->logger->info("File exists", ["path" => $fileDTO->getPath()]);
        } catch (FileNotFoundException) {
            $file = $this->fileBuilder->buildFromFileDTO($fileDTO);
            $this->logger->info("File create", ["path" => $fileDTO->getPath()]);
            $this->entityManager->persist($file);
        }

        $hasPath = false;
        foreach ($file->getPaths() as $path) {
            $this->syncFilePathStatus($path);

            $hasPath = $hasPath || $path->getPath() === $this->realPathFormatter->stripPrefix($fileDTO->getPath());
        }

        if (!$hasPath) {
            $file->addPath($this->pathBuilder->buildFromDTO($fileDTO));
            $this->logger->info("Path create", ["path" => $fileDTO->getPath()]);
        } else {
            $this->logger->info("Path exists", ["path" => $fileDTO->getPath()]);
        }

        $this->createThumbnails($file);
    }

    /**
     * @throws Exception
     */
    private function syncFilePathStatus(Path $path): void
    {
        $pathDTO = new PathDTO($this->realPathFormatter->prependPrefix($path->getPath()));
        if ($this->fsChecker->check($pathDTO)) {
            $path->setStatus(Status::ACTIVE);
        } else {
            $path->setStatus(Status::MISSED);
        }
    }

    private function createThumbnails(File $file): void
    {
        foreach ($file->getPaths() as $path) {
            $pathInfo = pathinfo($path->getPath());
            $extension = strtolower($pathInfo['extension'] ?? '');

            switch ($extension) {
                case 'pdf':
                    (new Process([
                        "pdftocairo",
                        $this->realPathFormatter->prependPrefix($path->getPath()),
                        "-singlefile",
                        "-q",
                        "-png",
                        $this->realPathFormatter->prependPrefix($path->getPath()),
                    ]))->run();
                    break;
                case 'djvu':
                    (new Process([
                        "ddjvu",
                        $this->realPathFormatter->prependPrefix($path->getPath()),
                        $this->realPathFormatter->prependPrefix($path->getPath()) . ".png",
                        "-page=1",
                    ]))->run();
                    break;
            }
        }
    }
}
