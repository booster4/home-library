<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Default\File\Path;
use App\Enum\File\Path\Status;
use App\Repository\Exceptions\FilePathNotFoundException;
use App\Repository\FilePathRepositoryInterface;
use App\Service\Utils\RealPathFormatter;
use Carbon\Carbon;
use Psr\Log\LoggerInterface;
use Symfony\Component\Uid\Uuid;

class FilePathService
{
    public function __construct(
        private readonly FilePathRepositoryInterface $filePathRepository,
        private readonly RealPathFormatter $realPathFormatter,
        private readonly LoggerInterface $logger,
    ) {
    }

    /**
     * @throws FilePathNotFoundException
     */
    public function removeByID(Uuid $id): bool // todo перенести в walker
    {
        $path = $this->filePathRepository->getByID($id);
        $realPath = $this->realPathFormatter->prependPrefix($path->getPath());

        $removed = unlink($realPath);
        if ($removed) {
            $path
                ->setStatus(Status::DELETE)
                ->setUpdatedAt(Carbon::now());
        } else {
            $this->logger->error("Remove file error", [
                "path" => $realPath,
                "id" => $id->toRfc4122(),
            ]);
        }

        return $removed;
    }
}
