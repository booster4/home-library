<?php

namespace App\Service\Utils;

use Exception;

class RealPathFormatter implements RealPathFormatterInterface
{
    private readonly string $prefix;

    /**
     * @throws Exception
     */
    public function __construct(TypedParameterBagReader $typedParameterBagReader)
    {
        $this->prefix = $typedParameterBagReader->getString("library.path");
    }

    public function stripPrefix(string $path): string
    {
        if (str_starts_with($path, $this->prefix)) {
            $path = mb_substr($path, mb_strlen($this->prefix, "UTF-8"), encoding: "UTF-8");
        }

        return $path;
    }

    public function prependPrefix(string $path): string
    {
        if (!str_starts_with($path, $this->prefix)) {
            $path = $this->prefix . $path;
        }

        return $path;
    }
}
