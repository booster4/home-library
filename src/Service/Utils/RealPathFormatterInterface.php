<?php

namespace App\Service\Utils;

interface RealPathFormatterInterface
{
    public function stripPrefix(string $path): string;

    public function prependPrefix(string $path): string;
}
