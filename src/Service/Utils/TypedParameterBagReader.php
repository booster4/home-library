<?php

declare(strict_types=1);

namespace App\Service\Utils;

use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class TypedParameterBagReader
{
    public function __construct(private readonly ParameterBagInterface $parameterBag)
    {
    }

    /**
     * @throws Exception
     */
    public function getString(string $key): string
    {
        $value = $this->parameterBag->get($key);
        if (!is_string($value)) {
            throw new Exception(
                sprintf(
                    "Unexpected type key %s. Should be declared as string, %s provided",
                    $key,
                    gettype($value),
                )
            );
        }

        return $value;
    }
}
