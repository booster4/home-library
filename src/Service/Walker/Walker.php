<?php

declare(strict_types=1);

namespace App\Service\Walker;

use App\Service\FileService;
use App\Service\Walker\DTO\FileDTO;
use App\Service\Walker\Factory\FileDTOFactoryInterface;
use App\Service\Walker\FileChecker\FileCheckerInterface;
use App\Service\Walker\FileStorageReader\FileStorageReaderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Iterator;
use Psr\Log\LoggerInterface;
use SplFileInfo;

/**
 * File library scanner.
 * Sequently walk through storage to handle files.
 */
class Walker implements WalkerInterface
{
    /**
     * @var Iterator<mixed, SplFileInfo>
     */
    private Iterator $files;

    public function __construct(
        private readonly FileService $fileService,
        private readonly EntityManagerInterface $entityManager,
        private readonly FileDTOFactoryInterface $fileDTOFactory,
        private readonly FileCheckerInterface $fileCompatibleChecker,
        private readonly FileStorageReaderInterface $fileStorageReader,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function walk(): void
    {
        $this->files = $this->fileStorageReader->provideFileIterator();

        iterator_apply($this->files, $this->handleFile(...));
    }

    private function handleFile(): true
    {
        $fileInfo = $this->files->current();

        try {
            $fileDTO = $this->fileDTOFactory->buildFromSplFileInfo($fileInfo);

            if ($this->fileCompatibleChecker->check($fileDTO)) {
                /** @var FileDTO $fileDTO */
                $this->fileService->save($fileDTO);
                $this->entityManager->flush();
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [
                'path' => $fileInfo->getPathname(),
            ]);
        }

        return true;
    }
}
