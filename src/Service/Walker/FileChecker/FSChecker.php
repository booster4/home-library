<?php

declare(strict_types=1);

namespace App\Service\Walker\FileChecker;

use App\Service\Walker\DTO\FileDTOInterface;

class FSChecker implements FileCheckerInterface
{
    public function check(FileDTOInterface $fileDTO): bool
    {
        return file_exists($fileDTO->getPath());
    }
}
