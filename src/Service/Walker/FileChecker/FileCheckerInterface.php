<?php

declare(strict_types=1);

namespace App\Service\Walker\FileChecker;

use App\Service\Walker\DTO\FileDTOInterface;
use Exception;

interface FileCheckerInterface
{
    /**
     * @throws Exception
     */
    public function check(FileDTOInterface $fileDTO): bool; // todo return DTO with reason
}
