<?php

declare(strict_types=1);

namespace App\Service\Walker\FileChecker;

use App\Service\Walker\DTO\FileDTOInterface;
use Exception;

class BookChecker implements FileCheckerInterface
{
    private const AVAILABLE_EXTENSIONS = [
        'pdf',
        'epub',
        'djvu',
        'fb2',
        'rtf',
    ];

    private const AVAILABLE_MIME_TYPE = [
        'application/epub+zip',
        'application/pdf',
        'application/zip',
        'image/vnd.djvu',
        'text/rtf',
        'text/xml',
    ];

    private const MAX_UNAVAILABLE_EXAMPLES = 10;

    /**
     * @var array<string, string[]>
     */
    private array $unavailableMimeTypes = [];

    /**
     * @var array<string, string[]>
     */
    private array $unavailableExtensions = [];

    public function check(FileDTOInterface $fileDTO): bool
    {
        return match (true) {
            $fileDTO->isDirectory() => false,
            null === $fileDTO->isDirectory() => false,
            !$this->checkExtension($fileDTO) => false,
            !$this->checkMimeType($fileDTO) => false,
            default => true
        };
    }

//    public function getUnavailableMimeTypes(): array
//    {
//        return $this->unavailableMimeTypes;
//    }
//
//    public function getUnavailableExtensions(): array
//    {
//        return $this->unavailableExtensions;
//    }

    private function checkExtension(FileDTOInterface $file): bool
    {
        $extension = $file->getExtension();
        if (
            null !== $extension &&
            !in_array($extension, self::AVAILABLE_EXTENSIONS)
        ) {
            $this->unavailableExtensions[$extension] ??= [];

            if (count($this->unavailableExtensions[$extension]) < self::MAX_UNAVAILABLE_EXAMPLES) {
                $this->unavailableExtensions[$extension][] = $file->getPath();
            }

            return false;
        }

        return true;
    }

    /**
     * @throws Exception
     */
    private function checkMimeType(FileDTOInterface $file): bool
    {
        $mime = $file->getMime();
        if (null !== $mime && !in_array($mime, self::AVAILABLE_MIME_TYPE)) {
            $this->unavailableMimeTypes[$mime] ??= [];

            if (count($this->unavailableMimeTypes[$mime]) < self::MAX_UNAVAILABLE_EXAMPLES) {
                $this->unavailableMimeTypes[$mime][] = $file->getPath();
            }

            return false;
        }

        return true;
    }
}
