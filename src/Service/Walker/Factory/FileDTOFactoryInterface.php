<?php

declare(strict_types=1);

namespace App\Service\Walker\Factory;

use App\Service\Walker\DTO\FileDTOInterface;
use Exception;
use SplFileInfo;

interface FileDTOFactoryInterface
{
    /**
     * @throws Exception
     */
    public function buildFromSplFileInfo(SplFileInfo $fileInfo): FileDTOInterface;
}
