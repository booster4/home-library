<?php

declare(strict_types=1);

namespace App\Service\Walker\Factory;

use App\Service\Walker\DTO\DirectoryDTO;
use App\Service\Walker\DTO\FileDTO;
use App\Service\Walker\DTO\FileDTOInterface;
use App\Service\Walker\FileHasher\FileHasherInterface;
use Exception;
use SplFileInfo;

class FileDTOFactory implements FileDTOFactoryInterface
{
    public function __construct(private readonly FileHasherInterface $fileHasher)
    {
    }

    public function buildFromSplFileInfo(SplFileInfo $fileInfo): FileDTOInterface
    {
        $path = $fileInfo->getPathname();
        if (is_dir($path)) {
            return new DirectoryDTO($path);
        }

        $size = $fileInfo->getSize();
        if (false === $size) {
            throw new Exception("Can't get size by file $path");
        }

        $mime = mime_content_type($path);
        if (false === $mime) {
            // @codeCoverageIgnoreStart
            throw new Exception("Can't get mime by file $path");
            // @codeCoverageIgnoreEnd
        }

        $fileHashResult = $this->fileHasher->hash($path);

        return new FileDTO(
            $path,
            mb_strtolower($fileInfo->getExtension(), 'UTF-8'),
            mb_strtolower($mime, 'UTF-8'),
            $fileHashResult->getHash(),
            $fileHashResult->getMethod(),
            $size
        );
    }
}
