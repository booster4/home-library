<?php

declare(strict_types=1);

namespace App\Service\Walker\DTO;

use App\Enum\File\FileHashMethod;

class FileDTO implements FileDTOInterface
{
    public function __construct(
        private readonly string $path,
        private readonly string $extension,
        private readonly string $mime,
        private readonly string $hash,
        private readonly FileHashMethod $hashMethod,
        private readonly int $size,
    ) {
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function isDirectory(): false
    {
        return false;
    }

    public function getExtension(): string
    {
        return $this->extension;
    }

    public function getMime(): string
    {
        return $this->mime;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function getHashMethod(): FileHashMethod
    {
        return $this->hashMethod;
    }

    public function getSize(): int
    {
        return $this->size;
    }
}
