<?php

declare(strict_types=1);

namespace App\Service\Walker\DTO;

class PathDTO implements FileDTOInterface
{
    public function __construct(protected readonly string $path)
    {
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function isDirectory(): null
    {
        return null;
    }

    public function getExtension(): null
    {
        return null;
    }

    public function getMime(): null
    {
        return null;
    }

    public function getHash(): null
    {
        return null;
    }

    public function getHashMethod(): null
    {
        return null;
    }

    public function getSize(): null
    {
        return null;
    }
}
