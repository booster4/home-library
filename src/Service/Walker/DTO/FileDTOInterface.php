<?php

declare(strict_types=1);

namespace App\Service\Walker\DTO;

use App\Enum\File\FileHashMethod;

interface FileDTOInterface
{
    public function getPath(): string;

    public function isDirectory(): ?bool;

    public function getExtension(): ?string;

    public function getMime(): ?string;

    public function getHash(): ?string;

    public function getHashMethod(): ?FileHashMethod;

    public function getSize(): ?int;
}
