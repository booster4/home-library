<?php

declare(strict_types=1);

namespace App\Service\Walker\FileHasher;

use App\Enum\File\FileHashMethod;

interface FileHasherResultInterface
{
    public function getHash(): string;

    public function getMethod(): FileHashMethod;
}
