<?php

declare(strict_types=1);

namespace App\Service\Walker\FileHasher;

class MD5FileHasher implements FileHasherInterface
{
    public function hash(string $path): FileHasherResultInterface
    {
        return new MD5FileHasherResult(md5_file($path));
    }
}
