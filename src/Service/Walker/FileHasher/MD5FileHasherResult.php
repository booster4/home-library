<?php

declare(strict_types=1);

namespace App\Service\Walker\FileHasher;

use App\Enum\File\FileHashMethod;

class MD5FileHasherResult implements FileHasherResultInterface
{
    public function __construct(private readonly string $hash)
    {
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function getMethod(): FileHashMethod
    {
        return FileHashMethod::BODY_MD5;
    }
}
