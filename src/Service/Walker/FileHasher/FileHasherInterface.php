<?php

declare(strict_types=1);

namespace App\Service\Walker\FileHasher;

interface FileHasherInterface
{
    public function hash(string $path): FileHasherResultInterface;
}
