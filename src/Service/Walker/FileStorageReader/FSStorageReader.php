<?php

declare(strict_types=1);

namespace App\Service\Walker\FileStorageReader;

use App\Service\Walker\DTO\PathDTO;
use App\Service\Walker\FileChecker\DirectoryChecker;
use App\Service\Walker\FileChecker\FileCheckerInterface;
use Exception;
use FilesystemIterator;
use Iterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class FSStorageReader implements FileStorageReaderInterface
{
    private readonly FileCheckerInterface $directoryChecker;

    public function __construct(
        private readonly FSStorageCredentials $credentials,
    ) {
        $this->directoryChecker = new DirectoryChecker();
    }

    /**
     * @rec
     * @throws Exception
     */
    public function provideFileIterator(): Iterator
    {
        $this->ping();

        return new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator(
                $this->credentials->getPath(),
                FilesystemIterator::CURRENT_AS_FILEINFO | FilesystemIterator::SKIP_DOTS
            )
        );
    }

    /**
     * @throws Exception
     */
    public function ping(): void
    {
        $pathDTO = new PathDTO($this->credentials->getPath());

        if (!$this->directoryChecker->check($pathDTO)) {
            throw new Exception("Directory {$pathDTO->getPath()} not found");
        }
    }
}
