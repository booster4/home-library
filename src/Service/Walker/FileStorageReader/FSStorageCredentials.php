<?php

declare(strict_types=1);

namespace App\Service\Walker\FileStorageReader;

class FSStorageCredentials implements FileStorageCredentialsInterface
{
    public function __construct(private readonly string $path)
    {
    }

    public function getPath(): string
    {
        return $this->path;
    }
}
