<?php

declare(strict_types=1);

namespace App\Service\Walker\FileStorageReader;

use Exception;
use Iterator;
use SplFileInfo;

interface FileStorageReaderInterface
{
    /**
     * @return Iterator<mixed, SplFileInfo>
     */
    public function provideFileIterator(): Iterator;

    /**
     * @throws Exception
     */
    public function ping(): void;
}
