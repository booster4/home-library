<?php

declare(strict_types=1);

namespace App\Service\Walker\FileStorageReader;

interface FileStorageCredentialsInterface
{
    public function getPath(): string;
}
