<?php

declare(strict_types=1);

namespace App\Service\Walker;

interface WalkerInterface
{
    public function walk(): void;
}
