<?php

namespace App\Controller\Files;

use App\Repository\FileRepositoryInterface;
use App\Repository\Filter\ListFilter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    public function __construct(private readonly FileRepositoryInterface $fileRepository)
    {
    }

    #[Route(path: '/files', name: 'fileList', methods: [Request::METHOD_GET])]
    public function __invoke(ListControllerRequest $request): Response
    {
        $limit = 100;
        $offset = ($request->getPage() - 1) * $limit;

        $filter = (new ListFilter())
            ->setHasBook(false)
            ->setLimit($limit + 1)
            ->setOffset($offset);
        $files = $this->fileRepository->getList($filter);

        return $this->render('files/list.html.twig', [
            'files' => array_slice($files, 0, $limit),
            'hasNextPage' => count($files) > $limit,
            'page' => $request->getPage(),
        ]);
    }
}
