<?php

namespace App\Controller\Files;

use RequestMapperBundle\DTO\MappableRequestInterface;
use Symfony\Component\Serializer\Annotation\Groups;

class ListControllerRequest implements MappableRequestInterface
{
    #[Groups("get")]
    private int $page = 1;

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }
}
