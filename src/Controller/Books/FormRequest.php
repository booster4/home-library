<?php

declare(strict_types=1);

namespace App\Controller\Books;

use RequestMapperBundle\DTO\MappableRequestInterface;
use Symfony\Component\Serializer\Annotation\Groups;

class FormRequest implements MappableRequestInterface
{
    #[Groups("path")]
    private string $fileID;

    public function getFileID(): string
    {
        return $this->fileID;
    }

    public function setFileID(string $fileID): self
    {
        $this->fileID = $fileID;

        return $this;
    }
}
