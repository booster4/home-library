<?php

declare(strict_types=1);

namespace App\Controller\Books;

use RequestMapperBundle\DTO\MappableRequestInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

class DeletePathRequest implements MappableRequestInterface
{
    #[Groups("post")]
    public Uuid $pathID;

    #[Groups("header")]
    public array $referer;

    public function getPathID(): Uuid
    {
        return $this->pathID;
    }

    public function setPathID(Uuid $pathID): self
    {
        $this->pathID = $pathID;

        return $this;
    }

    public function getReferer(): array
    {
        return $this->referer;
    }

    public function setReferer(array $referer): self
    {
        $this->referer = $referer;

        return $this;
    }
}
