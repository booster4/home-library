<?php

declare(strict_types=1);

namespace App\Controller\Books;

use App\Enum\Book\Language;
use RequestMapperBundle\DTO\MappableRequestInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

class SaveRequest implements MappableRequestInterface
{
    #[Groups('post')]
    private Uuid $fileID;

    #[Groups('post')]
    private string $title;

    #[Groups('post')]
    private string $isbn;

    #[Groups('post')]
    private string $publisher;

    #[Groups('post')]
    private string $series;

    #[Groups('post')]
    private string $edition;

    #[Groups('post')]
    private int $year;

    /** @var string[] */
    #[Groups('post')]
    private array $authors;

    #[Groups('post')]
    private Language $language;

    public function getFileID(): Uuid
    {
        return $this->fileID;
    }

    public function setFileID(Uuid $fileID): self
    {
        $this->fileID = $fileID;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getISBN(): string
    {
        return $this->isbn;
    }

    public function setISBN(string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getAuthors(): array
    {
        return $this->authors;
    }

    public function setAuthors(array $authors): self
    {
        $this->authors = $authors;

        return $this;
    }

    public function getPublisher(): string
    {
        return $this->publisher;
    }

    public function setPublisher(string $publisher): self
    {
        $this->publisher = $publisher;

        return $this;
    }

    public function getSeries(): string
    {
        return $this->series;
    }

    public function setSeries(string $series): self
    {
        $this->series = $series;

        return $this;
    }

    public function getEdition(): string
    {
        return $this->edition;
    }

    public function setEdition(string $edition): self
    {
        $this->edition = $edition;

        return $this;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }

    public function setLanguage(Language $language): self
    {
        $this->language = $language;

        return $this;
    }
}
