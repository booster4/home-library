<?php

namespace App\Controller\Books;

use App\Entity\Default\Book;
use App\Entity\Default\File\Path;
use App\Enum\File\Path\Status;
use App\Repository\Exceptions\FilePathNotFoundException;
use App\Repository\FileRepositoryInterface;
use App\Service\BookService;
use App\Service\BookService\DTO\BookDTO;
use App\Service\FilePathService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

class CreateFromFile extends AbstractController
{
    public function __construct(
        private readonly FileRepositoryInterface $fileRepository,
        private readonly FilePathService $filePathService,
        private readonly BookService $bookService,
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    #[Route(path: '/create-from-file/{fileID}', name: 'createBookFromFile', methods: [Request::METHOD_GET])]
    public function form(FormRequest $request): Response
    {
        $id = Uuid::fromRfc4122($request->getFileID());
        $file = $this->fileRepository->getByID($id);
        $paths = $file->getPaths()->filter(fn (Path $path) => $path->getStatus() === Status::ACTIVE);

        return $this->render('book/form.html.twig', [
            'file' => $file,
            'paths' => $paths,
        ]);
    }

    #[Route(path: '/create-from-file', name: 'saveBook', methods: [Request::METHOD_POST])]
    public function save(SaveRequest $request): Response
    {
        $this->entityManager->beginTransaction();

        try {
            $dto = $this->provideDTOFromSaveRequest($request);
            $this->bookService->create($dto);

            $this->entityManager->commit();
            $this->entityManager->flush();

            return $this->redirect($this->generateUrl('fileList'));
        } catch (Exception $e) {
            $this->entityManager->rollback();

            return $this->redirect($this->generateUrl('createBookFromFile', ['fileID' => $request->getFileID()]));
        }
    }

    /**
     * @throws FilePathNotFoundException
     */
    #[Route(path: '/delete-path-by-id', name: 'deletePathByID', methods: [Request::METHOD_POST])]
    public function deletePathByID(DeletePathRequest $request): Response
    {
        $this->entityManager->beginTransaction();

        if ($this->filePathService->removeByID($request->getPathID())) {  // todo add fallback
            $this->entityManager->flush();
            $this->entityManager->commit();
        } else {
            $this->entityManager->rollback();
        }

        if (count($request->getReferer()) > 0) {
            return $this->redirect($request->getReferer()[0]);
        }

        return $this->redirect($this->generateUrl("fileList"));
    }

    private function provideDTOFromSaveRequest(SaveRequest $request): BookDTO
    {
        return (new BookDTO())
            ->setFileID($request->getFileID())
            ->setTitle($request->getTitle())
            ->setISBN($request->getISBN())
            ->setLanguage($request->getLanguage())
            ->setSeries($request->getSeries())
            ->setYear($request->getYear())
            ->setPublisher($request->getPublisher())
            ->setEdition($request->getEdition())
            ->setAuthors($request->getAuthors());
    }
}
