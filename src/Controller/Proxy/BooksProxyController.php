<?php

namespace App\Controller\Proxy;

use App\Repository\Exceptions\FilePathNotFoundException;
use App\Repository\FilePathRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

class BooksProxyController extends AbstractController
{
    public function __construct(private readonly FilePathRepositoryInterface $filePathRepository)
    {
    }

    /**
     * @throws FilePathNotFoundException
     */
    #[Route(path: '/proxy/{id}', name: 'bookProxy', methods: [Request::METHOD_GET])]
    public function byID(Uuid $id): Response
    {
        $path = $this->filePathRepository->getByID($id);

        return new Response(headers: [
            "Content-Type" => $path->getFile()->getMime(),
            "X-Accel-Redirect" => sprintf("/books/%s", $path->getPath()),
            "Content-Disposition" => sprintf('inline; filename="%s"', basename($path->getPath())),
        ]);
    }

    #[Route(path: '/proxy/{id}/png', name: 'bookThumbnailProxy', methods: [Request::METHOD_GET])]
    public function thumbnailByID(Uuid $id): Response
    {
        $path = $this->filePathRepository->getByID($id);

        return new Response(headers: [
            "Content-Type" => $path->getFile()->getMime(),
            "X-Accel-Redirect" => sprintf("/books/%s.png", $path->getPath()),
            "Content-Disposition" => sprintf('inline; filename="%s.png"', basename($path->getPath())),
        ]);
    }
}
