<?php

declare(strict_types=1);

namespace App\EventListener\Command;

use App\Command\LoggableCommand;

trait NameBuilder
{
    private const DEFAULT_COMMAND_NAME = 'unnamed command (%s)';

    protected function provideCommandName(LoggableCommand $command): string
    {
        return $command->getName() ?: sprintf(self::DEFAULT_COMMAND_NAME, $command::class);
    }
}
