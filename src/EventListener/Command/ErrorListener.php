<?php

declare(strict_types=1);

namespace App\EventListener\Command;

use App\Command\LoggableCommand;
use App\Enum\CommandRun\Status;
use App\Repository\CommandRunRepositoryInterface;
use App\Repository\Exceptions\CommandRunNotFoundByRunIDException;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Event\ConsoleErrorEvent;

class ErrorListener
{
    use NameBuilder;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly CommandRunRepositoryInterface $commandRunRepository,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function __invoke(ConsoleErrorEvent $event): void
    {
        $command = $event->getCommand();
        if (!$command instanceof LoggableCommand) {
            return;
        }

        try {
            $commandRun = $this->commandRunRepository->getByRunID($command->getID());
            $commandRun
                ->setStatus(Status::ERROR)
                ->setDoneAt(Carbon::now());

            $this->entityManager->flush();
        } catch (CommandRunNotFoundByRunIDException $exception) {
            $this->logger->warning($exception->getMessage());

            return;
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());

            return;
        }

        $this->logger->warning(
            sprintf(
                "Command %s error: %s",
                $this->provideCommandName($command),
                $event->getError()->getMessage(),
            ),
            ['error' => $event->getError()],
        );
    }
}
