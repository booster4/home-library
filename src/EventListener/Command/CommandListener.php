<?php

declare(strict_types=1);

namespace App\EventListener\Command;

use App\Command\LoggableCommand;
use App\Entity\Default\CommandRun as CommandRunEntity;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Event\ConsoleCommandEvent;

class CommandListener
{
    use NameBuilder;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function __invoke(ConsoleCommandEvent $event): void
    {
        $command = $event->getCommand();
        if (!$command instanceof LoggableCommand) {
            return;
        }

        $name = $this->provideCommandName($command);

        $this->logger->info(
            sprintf(
                "Run loggable command %s with ID %s",
                $name,
                $command->getID()->toRfc4122(),
            ),
        );

        $commandRun = (new CommandRunEntity())
            ->setRunID($command->getID())
            ->setName($name);

        $this->entityManager->persist($commandRun);
        $this->entityManager->flush();
    }
}
