<?php

declare(strict_types=1);

namespace App\EventListener\Command;

use App\Command\LoggableCommand;
use App\Enum\CommandRun\Status;
use App\Repository\CommandRunRepositoryInterface;
use App\Repository\Exceptions\CommandRunNotFoundByRunIDException;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;

class TerminateListener
{
    use NameBuilder;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly CommandRunRepositoryInterface $commandRunRepository,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function __invoke(ConsoleTerminateEvent $event): void
    {
        $command = $event->getCommand();
        if (!$command instanceof LoggableCommand) {
            return;
        }

        try {
            $commandRun = $this->commandRunRepository->getByRunID($command->getID());
            $commandRun
                ->setStatus($this->provideCommandRunStatus($event))
                ->setDoneAt(Carbon::now());

            $this->entityManager->flush();
        } catch (CommandRunNotFoundByRunIDException $exception) {
            $this->logger->warning($exception->getMessage());

            return;
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());

            return;
        }

        $this->logger->info(
            sprintf(
                "Command %s terminate with exit code %d",
                $this->provideCommandName($command),
                $event->getExitCode(),
            ),
        );
    }

    /**
     * @throws Exception
     */
    private function provideCommandRunStatus(ConsoleTerminateEvent $event): Status
    {
        return match ($event->getExitCode()) {
            Command::SUCCESS => Status::COMPLETE,
            Command::FAILURE,
            Command::INVALID => Status::ERROR,
            default => throw new Exception(sprintf("Unexpected command exit code %d", $event->getExitCode()))
        };
    }
}
