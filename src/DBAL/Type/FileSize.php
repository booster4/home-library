<?php

declare(strict_types=1);

namespace App\DBAL\Type;

use App\ValueObject\FileSize as FileSizeVO;
use Doctrine\DBAL\ParameterType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Exception;

class FileSize extends Type
{
    final public const NAME = 'file_size';

    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getBigIntTypeDeclarationSQL($column);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?FileSizeVO
    {
        return $value === null ? null : new FileSizeVO($value);
    }

    /**
     * @throws Exception
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof FileSizeVO) {
            return (string) $value->getSize();
        }

        throw new Exception('Unexpected FileSize Value Object');
    }


    /**
     * @codeCoverageIgnore
     */
    public function getBindingType(): int
    {
        return ParameterType::INTEGER;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @codeCoverageIgnore
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
