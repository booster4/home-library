<?php

declare(strict_types=1);

namespace App\Factory\UuidFactory;

use Symfony\Component\Uid\Uuid;

class UuidFactory implements UuidFactoryInterface
{
    private readonly Uuid $namespace;

    public function __construct(string $namespaceUuid)
    {
        $this->namespace = Uuid::fromRfc4122($namespaceUuid);
    }

    public function buildV5(string|int|Uuid $seed): Uuid
    {
        return Uuid::v5($this->namespace, (string) $seed);
    }

    public function buildV7(): Uuid
    {
        return Uuid::v7();
    }
}
