<?php

namespace App\Factory;

use App\Entity\Default\Author;
use App\Factory\UuidFactory\UuidFactoryInterface;
use App\Service\Utils\TypedParameterBagReader;
use Exception;

class AuthorFactory
{
    private string $transliterator;

    /**
     * @throws Exception
     */
    public function __construct(
        private readonly UuidFactoryInterface $uuidFactory,
        TypedParameterBagReader $typedParameterBagReader,
    ) {
        $this->transliterator = $typedParameterBagReader->getString('transliterator');
    }

    public function build(string $name): Author
    {
        return (new Author())
            ->setID($this->uuidFactory->buildV7())
            ->setName($name)
            ->setTransliteration($this->transliterate($name));
    }

    private function transliterate(string $s): string
    {
        return transliterator_transliterate($this->transliterator, $s);
    }
}
