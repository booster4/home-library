<?php

declare(strict_types=1);

namespace App\Factory\FileStorageCredentialsFactory;

use App\Service\Utils\TypedParameterBagReader;
use App\Service\Walker\FileStorageReader\FileStorageCredentialsInterface;
use App\Service\Walker\FileStorageReader\FSStorageCredentials;
use Exception;

class FSStorageCredentialsFactory
{
    private const PARAMETERS_PATH_KEY = 'library.path';

    public function __construct(private readonly TypedParameterBagReader $typedParameterBagReader)
    {
    }

    /**
     * @throws Exception
     */
    public function build(): FileStorageCredentialsInterface
    {
        $path = $this->typedParameterBagReader->getString(self::PARAMETERS_PATH_KEY);

        return new FSStorageCredentials($path);
    }
}
