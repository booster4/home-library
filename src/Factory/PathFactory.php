<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Default\File\Path;
use App\Factory\UuidFactory\UuidFactoryInterface;
use App\Service\Utils\RealPathFormatterInterface;
use App\Service\Walker\DTO\FileDTOInterface;
use Exception;

class PathFactory
{
    public function __construct(
        private readonly UuidFactoryInterface $uuidFactory,
        private readonly RealPathFormatterInterface $realPathFormatter,
    ) {
    }

    /**
     * @throws Exception
     */
    public function buildFromDTO(FileDTOInterface $file): Path
    {
        return (new Path())
            ->setID($this->uuidFactory->buildV7())
            ->setPath($this->realPathFormatter->stripPrefix($file->getPath()));
    }
}
