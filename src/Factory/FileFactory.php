<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Default\File;
use App\Factory\UuidFactory\UuidFactoryInterface;
use App\Service\Walker\DTO\FileDTO;
use App\ValueObject\FileSize;

class FileFactory
{
    public function __construct(
        private readonly UuidFactoryInterface $uuidFactory,
    ) {
    }

    public function buildFromFileDTO(FileDTO $file): File
    {
        return (new File())
            ->setID($this->uuidFactory->buildV7())
            ->setHash($file->getHash())
            ->setHashMethod($file->getHashMethod())
            ->setSize(new FileSize($file->getSize()))
            ->setMime($file->getMime());
    }
}
