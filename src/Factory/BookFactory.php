<?php

namespace App\Factory;

use App\Entity\Default\Author;
use App\Entity\Default\Book;
use App\Enum\Book\Language;
use App\Factory\UuidFactory\UuidFactoryInterface;
use App\Repository\AuthorRepositoryInterface;
use App\Repository\Exceptions\BookAuthorNotFoundException;
use App\Repository\Exceptions\FileNotFoundException;
use App\Repository\FileRepositoryInterface;
use Symfony\Component\Uid\Uuid;

class BookFactory
{
    public function __construct(
        private readonly UuidFactoryInterface $uuidFactory,
        private readonly AuthorFactory $authorFactory,
        private readonly AuthorRepositoryInterface $bookAuthorRepository,
        private readonly FileRepositoryInterface $fileRepository,
    ) {
    }

    /**
     * @param string[] $authors
     *
     * @throws FileNotFoundException
     */
    public function build(
        Uuid $fileID,
        string $title,
        ?string $isbn,
        Language $language,
        ?string $series,
        ?int $year,
        ?string $publisher,
        ?string $edition,
        array $authors,
    ): Book {
        $file = $this->fileRepository->getByID($fileID);

        $book = (new Book())
            ->setId($this->uuidFactory->buildV7())
            ->setFile($file)
            ->setTitle($title)
            ->setLanguage($language)
            ->setISBN($isbn)
            ->setSeries($series)
            ->setYear($year)
            ->setPublisher($publisher)
            ->setEdition($edition);

        foreach ($authors as $name) {
            try {
                $author = $this->bookAuthorRepository->getByName($name);
            } catch (BookAuthorNotFoundException) {
                $author = $this->authorFactory->build($name);
            }

            $book->addAuthor($author);
        }

        return $book;
    }
}
