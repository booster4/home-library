PATH_TO_ENV_TEMPLATE=$(PWD)/dev/env/.env.template
PATH_TO_ENV_LOCAL=$(PWD)/dev/env/.env.local
PATH_TO_ENV_TARGET=$(PWD)/dev/env/.env
PATH_TO_DOCKER_COMPOSE_TEMPLATE=$(PWD)/dev/docker/compose/docker-compose.template.yml
PATH_TO_DOCKER_COMPOSE_COMPILED=$(PWD)/dev/docker/compose/docker-compose.compiled.yml
PATH_TO_SCRIPTS=$(PWD)/dev/script
PATH_TO_IMAGES=$(PWD)/dev/docker/images
PATH_TO_SOURCE=$(PWD)
PATH_TO_VAR=$(PWD)/var
PATH_TO_VENDOR=$(PWD)/vendor

include $(PATH_TO_ENV_TARGET)
export

env:
	@cp ${PATH_TO_ENV_LOCAL} ${PATH_TO_ENV_TARGET} 2>/dev/null || :
    	@cp -n ${PATH_TO_ENV_TEMPLATE} ${PATH_TO_ENV_TARGET} 2>/dev/null || :
cfg:
	@make env
	@docker-compose \
     		--file=$(PATH_TO_DOCKER_COMPOSE_TEMPLATE) \
     		--env-file=$(PATH_TO_ENV_TARGET) \
     		config > $(PATH_TO_DOCKER_COMPOSE_COMPILED)

create-network:
	$(PATH_TO_SCRIPTS)/create-network.sh $(APPLICATION_NETWORK_NAME) "192.206.0"

build-fpm:
	@docker build \
		--ssh default \
		--progress=plain \
		-t ${APPLICATION_NAME}/fpm \
		-f ${PATH_TO_IMAGES}/fpm/Dockerfile .

build-php:
	@docker build \
		--ssh default \
		--progress=plain \
		-t ${APPLICATION_NAME}/php \
		-f ${PATH_TO_IMAGES}/php/Dockerfile .

build-composer:
	@docker build \
		--ssh default \
		--progress=plain \
		-t ${APPLICATION_NAME}/composer \
		-f ${PATH_TO_IMAGES}/composer/Dockerfile .

build:
	@$(MAKE) build-fpm
	@$(MAKE) build-php
	@$(MAKE) build-composer

up: cfg
	docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) up -d --build
	@make create-database
	@make migrate

down:
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) down

reset: drop-database down
	@rm -rf $(PATH_TO_SOURCE)/var/lib/postgresql
	@rm -rf $(PATH_TO_SOURCE)/var/lib/mariadb
	@rm -rf $(PATH_TO_SOURCE)/var/lib/redis

# Quality tools
phpstan:
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm vendor/bin/phpstan analyse -c phpstan.neon --xdebug --memory-limit=1024M

# Check PHP version
php-cs-fix-validate:
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm vendor/bin/php-cs-fixer fix --dry-run --diff --using-cache=no

phpcs:
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm vendor/bin/phpcs -p

phpmd:
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm vendor/bin/phpmd src/ text naming

psalm:
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm vendor/bin/psalm --no-cache

rector:
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm vendor/bin/rector --no-cache

code-quality-tools:
	@make phpstan
	@make psalm
	@make phpcs

php-metrics:
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm php ./vendor/bin/phpmetrics --junit=junit.xml --report-html=./report ./


# Tests
tests-unit:
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm bin/phpunit bundles/RequestMapperBundle/tests
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm bin/phpunit --testsuite Unit
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm bin/phpunit --testsuite Integrational

tests-functional:
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm bin/phpunit --testsuite Functional

tests-all:
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm bin/phpunit --log-junit junit.xml

# DB
migration-diff:
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm bin/console doctrine:migrations:diff --em=default --env=dev

migrate:
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm bin/console doctrine:migrations:migrate --em=default --no-interaction --env=dev
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm bin/console doctrine:migrations:migrate --em=default --no-interaction --env=test

create-database:
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm bin/console doctrine:database:create --if-not-exists --connection=default --env=dev
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm bin/console doctrine:database:create --if-not-exists --connection=default --env=test

drop-database:
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm bin/console doctrine:database:drop --if-exists --force --connection=default --env=dev
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm bin/console doctrine:database:drop --if-exists --force --connection=default --env=test

clear-database:
	@make drop-database
	@docker-compose --file=$(PATH_TO_DOCKER_COMPOSE_COMPILED) exec fpm bin/console doctrine:database:create
	@$(MAKE) migrate
