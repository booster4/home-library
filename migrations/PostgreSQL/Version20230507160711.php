<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230507160711 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE command_runs_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE authors (
            id UUID NOT NULL,
            name VARCHAR(255) NOT NULL,
            transliteration VARCHAR(255) NOT NULL,
            status VARCHAR(255) NOT NULL,
            created_at TIMESTAMP(6) WITHOUT TIME ZONE NOT NULL,
            updated_at TIMESTAMP(6) WITHOUT TIME ZONE DEFAULT NULL,
            PRIMARY KEY(id))
        ');
        $this->addSql('COMMENT ON COLUMN authors.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE books (
            id UUID NOT NULL,
            file_id UUID DEFAULT NULL,
            title TEXT NOT NULL,
            isbn VARCHAR(255) DEFAULT NULL,
            language VARCHAR(255) NOT NULL,
            publisher VARCHAR(255) DEFAULT NULL,
            series TEXT DEFAULT NULL,
            edition VARCHAR(255) DEFAULT NULL,
            year INT DEFAULT NULL,
            status VARCHAR(255) NOT NULL,
            created_at TIMESTAMP(6) WITHOUT TIME ZONE NOT NULL,
            updated_at TIMESTAMP(6) WITHOUT TIME ZONE DEFAULT NULL,
            PRIMARY KEY(id))
        ');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4A1B2A9293CB796C ON books (file_id)');
        $this->addSql('COMMENT ON COLUMN books.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN books.file_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE book_authors (
            book_id UUID NOT NULL,
            author_id UUID NOT NULL,
            PRIMARY KEY(book_id, author_id))
        ');
        $this->addSql('CREATE INDEX IDX_1D2C02C716A2B381 ON book_authors (book_id)');
        $this->addSql('CREATE INDEX IDX_1D2C02C7F675F31B ON book_authors (author_id)');
        $this->addSql('COMMENT ON COLUMN book_authors.book_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN book_authors.author_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE command_runs (
            id INT NOT NULL,
            name VARCHAR(255) NOT NULL,
            status VARCHAR(255) NOT NULL,
            run_id UUID NOT NULL,
            run_at TIMESTAMP(6) WITHOUT TIME ZONE NOT NULL,
            done_at TIMESTAMP(6) WITHOUT TIME ZONE DEFAULT NULL,
            PRIMARY KEY(id))
        ');
        $this->addSql('COMMENT ON COLUMN command_runs.run_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE file_paths (
            id UUID NOT NULL,
            file_id UUID NOT NULL,
            path TEXT NOT NULL,
            status VARCHAR(255) NOT NULL,
            created_at TIMESTAMP(6) WITHOUT TIME ZONE NOT NULL,
            updated_at TIMESTAMP(6) WITHOUT TIME ZONE DEFAULT NULL,
            PRIMARY KEY(id))
        ');
        $this->addSql('CREATE INDEX IDX_2139362693CB796C ON file_paths (file_id)');
        $this->addSql('COMMENT ON COLUMN file_paths.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN file_paths.file_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE files (
            id UUID NOT NULL,
            hash VARCHAR(255) NOT NULL,
            hash_method VARCHAR(255) NOT NULL,
            size BIGINT NOT NULL,
            status VARCHAR(255) NOT NULL,
            mime VARCHAR(255) NOT NULL,
            created_at TIMESTAMP(6) WITHOUT TIME ZONE NOT NULL,
            updated_at TIMESTAMP(6) WITHOUT TIME ZONE DEFAULT NULL,
            PRIMARY KEY(id))
        ');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6354059D1B862B8 ON files (hash)');
        $this->addSql('COMMENT ON COLUMN files.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN files.size IS \'(DC2Type:file_size)\'');
        $this->addSql('ALTER TABLE books 
            ADD CONSTRAINT FK_4A1B2A9293CB796C
            FOREIGN KEY (file_id)
            REFERENCES files (id) NOT DEFERRABLE INITIALLY IMMEDIATE
        ');
        $this->addSql('ALTER TABLE book_authors
            ADD CONSTRAINT FK_1D2C02C716A2B381
            FOREIGN KEY (book_id)
            REFERENCES books (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE
        ');
        $this->addSql('ALTER TABLE book_authors
            ADD CONSTRAINT FK_1D2C02C7F675F31B
            FOREIGN KEY (author_id)
            REFERENCES authors (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE
        ');
        $this->addSql('ALTER TABLE file_paths
            ADD CONSTRAINT FK_2139362693CB796C
            FOREIGN KEY (file_id)
            REFERENCES files (id) NOT DEFERRABLE INITIALLY IMMEDIATE
        ');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE command_runs_id_seq CASCADE');
        $this->addSql('ALTER TABLE books DROP CONSTRAINT FK_4A1B2A9293CB796C');
        $this->addSql('ALTER TABLE book_authors DROP CONSTRAINT FK_1D2C02C716A2B381');
        $this->addSql('ALTER TABLE book_authors DROP CONSTRAINT FK_1D2C02C7F675F31B');
        $this->addSql('ALTER TABLE file_paths DROP CONSTRAINT FK_2139362693CB796C');
        $this->addSql('DROP TABLE authors');
        $this->addSql('DROP TABLE books');
        $this->addSql('DROP TABLE book_authors');
        $this->addSql('DROP TABLE command_runs');
        $this->addSql('DROP TABLE file_paths');
        $this->addSql('DROP TABLE files');
    }
}
