<?php

declare(strict_types=1);

namespace App\Tests\Unit\Command;

use App\Command\ScanBooksDirectory;
use App\Factory\UuidFactory\UuidFactoryInterface;
use App\Service\Walker\WalkerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScanBooksDirectoryTest extends TestCase
{
    public function testExecute(): void
    {
        $walker = $this->createMock(WalkerInterface::class);
        $walker->expects($this->once())->method('walk');

        $uuidFactory = $this->createMock(UuidFactoryInterface::class);
        $uuidFactory->expects($this->once())->method('buildV7');

        $input = $this->createMock(InputInterface::class);
        $output = $this->createMock(OutputInterface::class);

        $command = new ScanBooksDirectory($walker, $uuidFactory);

        $command->execute($input, $output);
    }
}
