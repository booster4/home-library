<?php

declare(strict_types=1);

namespace App\Tests\Unit\Command;

use App\Command\LoggableCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\AbstractUid;
use Symfony\Component\Uid\Uuid;

class LoggableCommandTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $id = Uuid::v7();
        $command = new class extends LoggableCommand {
            public function setID(AbstractUid $id): void
            {
                parent::setID($id);
            }
        };

        $command->setID($id);

        $this->assertEquals($id, $command->getID());
    }
}
