<?php

declare(strict_types=1);

namespace App\Tests\Unit\EventListener\Command;

use App\Command\LoggableCommand;
use App\Entity\Default\CommandRun;
use App\Enum\CommandRun\Status;
use App\EventListener\Command\ErrorListener;
use App\EventListener\Command\TerminateListener;
use App\Repository\CommandRunRepositoryInterface;
use App\Repository\Exceptions\CommandRunNotFoundByRunIDException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Event\ConsoleErrorEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Uid\AbstractUid;
use Symfony\Component\Uid\Uuid;

class ErrorListenerTest extends TestCase
{
    public function testInvokeWithLoggableCommand(): void
    {
        $runID = Uuid::fromRfc4122('f1641fb2-709f-4fbe-9065-8e591c6c078e');

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->once())->method('flush');

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('warning');

        $commandRun = $this->createMock(CommandRun::class);
        $commandRun->expects($this->once())->method('setStatus')->with(Status::ERROR)->willReturn($commandRun);
        $commandRun->expects($this->once())->method('setDoneAt')->willReturn($commandRun);

        $commandRunRepository = $this->createMock(CommandRunRepositoryInterface::class);
        $commandRunRepository->expects($this->once())->method('getByRunID')->with($runID)->willReturn($commandRun);

        $command = new class extends LoggableCommand {
            public function getName(): string
            {
                return 'name';
            }

            public function getID(): AbstractUid
            {
                return Uuid::fromRfc4122('f1641fb2-709f-4fbe-9065-8e591c6c078e');
            }
        };

        $input = $this->createMock(InputInterface::class);
        $output = $this->createMock(OutputInterface::class);

        $event = new ConsoleErrorEvent($input, $output, new Exception('foo'), $command);

        $commandListener = new ErrorListener($entityManager, $commandRunRepository, $logger);

        $commandListener($event);
    }

    public function testInvokeWithCommand(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->never())->method('flush');

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->never())->method('info');

        $command = new class extends Command {
            public function getName(): string
            {
                return 'name';
            }
        };

        $commandRunRepository = $this->createMock(CommandRunRepositoryInterface::class);

        $input = $this->createMock(InputInterface::class);
        $output = $this->createMock(OutputInterface::class);

        $event = new ConsoleErrorEvent($input, $output, new Exception('foo'), $command);

        $commandListener = new ErrorListener($entityManager, $commandRunRepository, $logger);

        $commandListener($event);
    }

    public function testInvokeCommandHasNoCommandRun(): void
    {
        $runID = Uuid::fromRfc4122('f1641fb2-709f-4fbe-9065-8e591c6c078e');
        $message = 'not found';

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->never())->method('flush');

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('warning')->with($message);

        $commandRunRepository = $this->createMock(CommandRunRepositoryInterface::class);
        $commandRunRepository
            ->expects($this->once())
            ->method('getByRunID')
            ->with($runID)
            ->willThrowException(new CommandRunNotFoundByRunIDException($message));

        $command = new class extends LoggableCommand {
            public function getName(): string
            {
                return 'name';
            }

            public function getID(): AbstractUid
            {
                return Uuid::fromRfc4122('f1641fb2-709f-4fbe-9065-8e591c6c078e');
            }
        };

        $input = $this->createMock(InputInterface::class);
        $output = $this->createMock(OutputInterface::class);

        $event = new ConsoleErrorEvent($input, $output, new Exception('foo'), $command);

        $commandListener = new ErrorListener($entityManager, $commandRunRepository, $logger);

        $commandListener($event);
    }

    public function testInvokeCommandWithException(): void
    {
        $runID = Uuid::fromRfc4122('f1641fb2-709f-4fbe-9065-8e591c6c078e');
        $message = 'exception message';

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->never())->method('flush');

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('error')->with($message);

        $commandRunRepository = $this->createMock(CommandRunRepositoryInterface::class);
        $commandRunRepository
            ->expects($this->once())
            ->method('getByRunID')
            ->with($runID)
            ->willThrowException(new Exception($message));

        $command = new class extends LoggableCommand {
            public function getName(): string
            {
                return 'name';
            }

            public function getID(): AbstractUid
            {
                return Uuid::fromRfc4122('f1641fb2-709f-4fbe-9065-8e591c6c078e');
            }
        };

        $input = $this->createMock(InputInterface::class);
        $output = $this->createMock(OutputInterface::class);

        $event = new ConsoleErrorEvent($input, $output, new Exception('message'), $command);

        $commandListener = new ErrorListener($entityManager, $commandRunRepository, $logger);

        $commandListener($event);
    }
}
