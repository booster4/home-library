<?php

declare(strict_types=1);

namespace App\Tests\Unit\EventListener\Command;

use App\Command\LoggableCommand;
use App\Entity\Default\CommandRun;
use App\EventListener\Command\CommandListener;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Uid\AbstractUid;
use Symfony\Component\Uid\Uuid;

class CommandListenerTest extends TestCase
{
    public function testInvokeWithLoggableCommand(): void
    {
        $name = 'name';
        $runID = Uuid::fromRfc4122('f1641fb2-709f-4fbe-9065-8e591c6c078e');

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager
            ->expects($this->once())
            ->method('persist')
            ->with($this->callback(function (CommandRun $commandRun) use ($runID, $name) {
                $this->assertEquals($runID, $commandRun->getRunID());
                $this->assertEquals($name, $commandRun->getName());

                return true;
            }));
        $entityManager->expects($this->once())->method('flush');

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('info');

        $command = new class extends LoggableCommand {
            public function getName(): string
            {
                return 'name';
            }

            public function getID(): AbstractUid
            {
                return Uuid::fromRfc4122('f1641fb2-709f-4fbe-9065-8e591c6c078e');
            }
        };

        $input = $this->createMock(InputInterface::class);
        $output = $this->createMock(OutputInterface::class);

        $event = new ConsoleCommandEvent($command, $input, $output);

        $commandListener = new CommandListener($entityManager, $logger);

        $commandListener($event);
    }

    public function testInvokeWithCommand(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->never())->method('persist');
        $entityManager->expects($this->never())->method('flush');

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->never())->method('info');

        $command = new class extends Command {
            public function getName(): string
            {
                return 'name';
            }
        };

        $input = $this->createMock(InputInterface::class);
        $output = $this->createMock(OutputInterface::class);

        $event = new ConsoleCommandEvent($command, $input, $output);

        $commandListener = new CommandListener($entityManager, $logger);

        $commandListener($event);
    }
}
