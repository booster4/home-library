<?php

declare(strict_types=1);

namespace App\Tests\Unit\EventListener\Command;

use App\Command\LoggableCommand;
use App\EventListener\Command\NameBuilder;
use Generator;
use PHPUnit\Framework\TestCase;

class NameBuilderTest extends TestCase
{
    /**
     * @dataProvider provideCommand
     */
    public function testProvideCommandName(LoggableCommand $loggableCommand, string $regex): void
    {
        $listener = new class {
            use NameBuilder;

            public function publicProvideCommandName(LoggableCommand $command): string
            {
                return $this->provideCommandName($command);
            }
        };

        $name = $listener->publicProvideCommandName($loggableCommand);
        $this->assertTrue(preg_match($regex, $name) === 1);
    }

    public function provideCommand(): Generator
    {
        $name = 'cmd';
        $regex = "/^$name$/";
        $command = $this->createMock(LoggableCommand::class);
        $command->expects($this->once())->method('getName')->willReturn($name);

        yield 'command with name' => [$command, $regex];

        $regex = '/^unnamed command \(Mock_LoggableCommand_.*\)$/';
        $command = $this->createMock(LoggableCommand::class);
        $command->expects($this->once())->method('getName')->willReturn(null);

        yield 'command without name' => [$command, $regex];
    }
}
