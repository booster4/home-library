<?php

declare(strict_types=1);

namespace App\Tests\Unit\Builder;

use App\Enum\File\FileHashMethod;
use App\Factory\PathFactory;
use App\Factory\UuidFactory\UuidFactoryInterface;
use App\Service\Utils\RealPathFormatterInterface;
use App\Service\Walker\DTO\FileDTO;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class PathBuilderTest extends TestCase
{
    public function testBuild(): void
    {
        $path = 'path';

        $uuid = Uuid::v7();
        $uuidFactory = $this->createMock(UuidFactoryInterface::class);
        $uuidFactory->expects($this->once())->method('buildV7')->willReturn($uuid);

        $realPathFormatter = $this->createMock(RealPathFormatterInterface::class);
        $realPathFormatter->expects($this->once())->method('stripPrefix')->willReturn($path);

        $fileDTO = new FileDTO($path, '', '', '', FileHashMethod::BODY_MD5, 0);

        $pathBuilder = new PathFactory($uuidFactory, $realPathFormatter);
        $pathEntity = $pathBuilder->buildFromDTO($fileDTO);

        $this->assertEquals($path, $pathEntity->getPath());
    }
}
