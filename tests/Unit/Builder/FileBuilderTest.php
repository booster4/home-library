<?php

declare(strict_types=1);

namespace App\Tests\Unit\Builder;

use App\Enum\File\FileHashMethod;
use App\Factory\FileFactory;
use App\Factory\UuidFactory\UuidFactoryInterface;
use App\Service\Walker\DTO\FileDTO;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class FileBuilderTest extends TestCase
{
    public function testBuild(): void
    {
        $path = 'path';
        $extension = 'extension';
        $mime = 'mime';
        $hash = 'hash';
        $hashMethod = FileHashMethod::BODY_MD5;
        $size = 123;

        $uuid = Uuid::v7();
        $uuidFactory = $this->createMock(UuidFactoryInterface::class);
        $uuidFactory->expects($this->once())->method('buildV7')->willReturn($uuid);

        $fileDTO = (new FileDTO($path, $extension, $mime, $hash, $hashMethod, $size));

        $fileBuilder = new FileFactory($uuidFactory);
        $file = $fileBuilder->buildFromFileDTO($fileDTO);

        $this->assertEquals($mime, $file->getMime());
        $this->assertEquals($hash, $file->getHash());
        $this->assertEquals($hashMethod, $file->getHashMethod());
        $this->assertEquals($size, $file->getSize()->getSize());
    }
}
