<?php

declare(strict_types=1);

namespace App\Tests\Unit\ValueObject;

use App\ValueObject\FileSize;
use Exception;
use Generator;
use PHPUnit\Framework\TestCase;

class FileSizeTest extends TestCase
{
    public function testConstructor(): void
    {
        $size = 1000;
        $fileSize = new FileSize($size);

        $this->assertEquals($size, $fileSize->getSize());
        $this->assertTrue($fileSize->isStrict());

        $fileSize = new FileSize($size, false);

        $this->assertFalse($fileSize->isStrict());
    }

    /**
     * @dataProvider toStringDataProvider
     * @throws Exception
     */
    public function testToString(string $expected, int $bytes, bool $strict): void
    {
        $fileSize = new FileSize($bytes, $strict);

        $this->assertEquals($expected, (string) $fileSize);
    }

    public function toStringDataProvider(): Generator
    {
        yield ['expected' => '≈1000 B', 'bytes' => 1000, 'strict' => false];
        yield ['expected' => '1.00 KB', 'bytes' => 1024, 'strict' => true];
        yield ['expected' => '1.03 KB', 'bytes' => 1058, 'strict' => true];
        yield ['expected' => '1 B', 'bytes' => 1, 'strict' => true];
        yield ['expected' => '32.46 GB', 'bytes' => 34_857_387_453, 'strict' => true];
        yield ['expected' => '20.47 PB', 'bytes' => 23_045_825_717_874_561, 'strict' => true];
        yield ['expected' => '≈3.00 TB', 'bytes' => 3_298_534_883_328, 'strict' => false];
    }
}
