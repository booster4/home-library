<?php

declare(strict_types=1);

namespace App\Tests\Unit\Factory;

use App\Factory\AuthorFactory;
use App\Factory\UuidFactory\UuidFactoryInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class AuthorFactoryTest extends TestCase
{
    public function testBuild(): void
    {
        $uuid = Uuid::v7();

        $uuidFactory = $this->createMock(UuidFactoryInterface::class);
        $uuidFactory
            ->expects($this->once())
            ->method('buildV7')
            ->willReturn($uuid);

        $authorFactory = new AuthorFactory($uuidFactory);

        $name = 'Пушкин Александр Сергеевич';
        $transliteration = 'Pushkin Aleksandr Sergeyevich';

        $author = $authorFactory->build($name);

        $this->assertEquals($name, $author->getName());
        $this->assertEquals($transliteration, $author->getTransliteration());
    }
}
