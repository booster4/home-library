<?php

declare(strict_types=1);

namespace App\Tests\Unit\Factory;

use App\Factory\FileStorageCredentialsFactory\FSStorageCredentialsFactory;
use App\Service\Utils\TypedParameterBagReader;
use Exception;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class FSStorageCredentialsFactoryTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testBuild(): void
    {
        $path = 'path';

        $reflection = new ReflectionClass(FSStorageCredentialsFactory::class);
        $key = $reflection->getConstant('PARAMETERS_PATH_KEY');

        $typedParameterBagReader = $this->createMock(TypedParameterBagReader::class);
        $typedParameterBagReader
            ->expects($this->once())
            ->method('getString')
            ->with($key)
            ->willReturn($path);

        $factory = new FSStorageCredentialsFactory($typedParameterBagReader);
        $credentials = $factory->build();

        $this->assertEquals($path, $credentials->getPath());
    }
}
