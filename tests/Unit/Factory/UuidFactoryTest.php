<?php

declare(strict_types=1);

namespace App\Tests\Unit\Factory;

use App\Factory\UuidFactory\UuidFactory;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class UuidFactoryTest extends TestCase
{
    public function testBuildV5(): void
    {
        $factory = new UuidFactory('cc197ad6-cc9c-4dfd-9b67-c68b970fcc49');
        $uuid = $factory->buildV5('foo');

        $this->assertEquals('934c94b0-3f36-52dc-a90c-ac03075c2f9f', $uuid->toRfc4122());
    }

    public function testBuildV7(): void
    {
        $factory = new UuidFactory('cc197ad6-cc9c-4dfd-9b67-c68b970fcc49');
        $uuid = $factory->buildV7();

        $this->assertTrue(Uuid::isValid($uuid->toRfc4122()));
    }
}
