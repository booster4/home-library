<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\Utils;

use App\Service\Utils\TypedParameterBagReader;
use Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class TypedParameterBagReaderTest extends TestCase
{
    public function testGetString(): void
    {
        $key = 'key';
        $result = 'result';

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->once())->method('get')->with($key)->willReturn($result);

        $typedParameterBagReader = new TypedParameterBagReader($parameterBag);

        $this->assertEquals($result, $typedParameterBagReader->getString($key));
    }

    public function testGetStringWithException(): void
    {
        $key = 'key';
        $result = 123;

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->once())->method('get')->with($key)->willReturn($result);

        $typedParameterBagReader = new TypedParameterBagReader($parameterBag);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Unexpected type key $key. Should be declared as string, integer provided");
        $typedParameterBagReader->getString($key);
    }
}
