<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\Walker\FileStorage;

use App\Service\Walker\FileStorageReader\FSStorageCredentials;
use App\Service\Walker\FileStorageReader\FSStorageReader;
use Exception;
use PHPUnit\Framework\TestCase;
use SplFileInfo;

class FSStorageReaderTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testProvideFileIterator(): void
    {
        $path = __DIR__ . "/FSStorageReaderTest";
        $credentials = new FSStorageCredentials($path);

        $fsStorageReader = new FSStorageReader($credentials);
        $iterator = $fsStorageReader->provideFileIterator();

        $paths = [];

        /** @var SplFileInfo $file */
        foreach ($iterator as $file) {
            $paths[] = $file->getFilename();
        }

        $this->assertCount(4, $paths);
        $this->assertContains('first_file.pdf', $paths);
        $this->assertContains('second_file.pdf', $paths);
        $this->assertContains('third_file.pdf', $paths);
        $this->assertContains('fourth_file.txt', $paths);
    }

    public function testPing(): void
    {
        $path = __DIR__ . "/NotExistsTable";
        $credentials = new FSStorageCredentials($path);

        $fsStorageReader = new FSStorageReader($credentials);
        $this->expectException(Exception::class);
        $fsStorageReader->ping();
    }
}
