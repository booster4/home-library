<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\Walker\FileStorage;

use App\Service\Walker\FileStorageReader\FSStorageCredentials;
use PHPUnit\Framework\TestCase;

class FSStorageCredentialsTest extends TestCase
{
    public function testConstructor(): void
    {
        $path = 'path';
        $fsStorageCredentials = new FSStorageCredentials($path);

        $this->assertEquals($path, $fsStorageCredentials->getPath());
    }
}
