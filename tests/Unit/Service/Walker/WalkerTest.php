<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\Walker;

use App\Service\FileService;
use App\Service\Walker\Factory\FileDTOFactory;
use App\Service\Walker\Factory\FileDTOFactoryInterface;
use App\Service\Walker\FileChecker\BookChecker;
use App\Service\Walker\FileChecker\FileCheckerInterface;
use App\Service\Walker\FileHasher\MD5FileHasher;
use App\Service\Walker\FileStorageReader\FileStorageReaderInterface;
use App\Service\Walker\FileStorageReader\FSStorageCredentials;
use App\Service\Walker\FileStorageReader\FSStorageReader;
use App\Service\Walker\Walker;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class WalkerTest extends TestCase
{
    public function testWalk(): void
    {
        $fileService = $this->createMock(FileService::class);
        $fileService->expects($this->exactly(3))->method('save');

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->exactly(3))->method('flush');

        $fileDTOFactory = new FileDTOFactory(new MD5FileHasher());
        $fileChecker = new BookChecker();

        $fileStorageCredentials = new FSStorageCredentials(__DIR__ . "/WalkerTest");
        $fileStorage = new FSStorageReader($fileStorageCredentials);

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->never())->method('error');

        $walker = new Walker(
            $fileService,
            $entityManager,
            $fileDTOFactory,
            $fileChecker,
            $fileStorage,
            $logger,
        );

        $walker->walk();
    }

    public function testWalkException(): void
    {
        $fileService = $this->createMock(FileService::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);

        $fileDTOFactory = $this->createMock(FileDTOFactory::class);
        $fileDTOFactory
            ->expects($this->exactly(4))
            ->method('buildFromSplFileInfo')
            ->willThrowException(new Exception("Can't get size by file"));

        $fileChecker = new BookChecker();

        $fileStorageCredentials = new FSStorageCredentials(__DIR__ . "/WalkerTest");
        $fileStorage = new FSStorageReader($fileStorageCredentials);

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->exactly(4))->method('error');

        $walker = new Walker(
            $fileService,
            $entityManager,
            $fileDTOFactory,
            $fileChecker,
            $fileStorage,
            $logger,
        );

        $walker->walk();
    }


}
