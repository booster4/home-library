<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\Walker\Factory;

use App\Enum\File\FileHashMethod;
use App\Service\Walker\DTO\DirectoryDTO;
use App\Service\Walker\DTO\FileDTO;
use App\Service\Walker\Factory\FileDTOFactory;
use App\Service\Walker\FileHasher\MD5FileHasher;
use App\Service\Walker\FileHasher\MD5FileHasherResult;
use Exception;
use PHPUnit\Framework\TestCase;
use SplFileInfo;

class FileDTOFactoryTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testBuildFromSplFileInfoFile(): void
    {
        $path = __FILE__;
        $extension = 'extension';
        $size = 100;
        $mime = 'text/x-php';
        $hash = 'hash';
        $hashMethod = FileHashMethod::BODY_MD5;

        $fileInfo = $this->createMock(SplFileInfo::class);
        $fileInfo->expects($this->once())->method('getSize')->willReturn($size);
        $fileInfo->expects($this->once())->method('getPathname')->willReturn($path);
        $fileInfo->expects($this->once())->method('getExtension')->willReturn($extension);

        $hasher = $this->createMock(MD5FileHasher::class);
        $hasher
            ->expects($this->once())
            ->method('hash')
            ->with($path)
            ->willReturn(new MD5FileHasherResult($hash));

        $fileDTOFactory = new FileDTOFactory($hasher);
        $fileDTO = $fileDTOFactory->buildFromSplFileInfo($fileInfo);

        $this->assertInstanceOf(FileDTO::class, $fileDTO);
        $this->assertFalse($fileDTO->isDirectory());
        $this->assertEquals($path, $fileDTO->getPath());
        $this->assertEquals($hash, $fileDTO->getHash());
        $this->assertEquals($hashMethod, $fileDTO->getHashMethod());
        $this->assertEquals($size, $fileDTO->getSize());
        $this->assertEquals($extension, $fileDTO->getExtension());
        $this->assertEquals($mime, $fileDTO->getMime());
    }

    /**
     * @throws Exception
     */
    public function testBuildFromSplFileInfoDirectory(): void
    {
        $path = __DIR__;

        $fileInfo = $this->createMock(SplFileInfo::class);
        $fileInfo->expects($this->once())->method('getPathname')->willReturn($path);
        $fileInfo->expects($this->never())->method('getSize');
        $fileInfo->expects($this->never())->method('getExtension');

        $hasher = $this->createMock(MD5FileHasher::class);
        $hasher
            ->expects($this->never())
            ->method('hash');

        $fileDTOFactory = new FileDTOFactory($hasher);
        $fileDTO = $fileDTOFactory->buildFromSplFileInfo($fileInfo);

        $this->assertInstanceOf(DirectoryDTO::class, $fileDTO);
        $this->assertTrue($fileDTO->isDirectory());
        $this->assertEquals($path, $fileDTO->getPath());
        $this->assertNull($fileDTO->getHash());
        $this->assertNull($fileDTO->getHashMethod());
        $this->assertNull($fileDTO->getSize());
        $this->assertNull($fileDTO->getExtension());
        $this->assertNull($fileDTO->getMime());
    }

    public function testBuildFromSplFileInfoSizeException(): void
    {
        $path = __DIR__ . '/emptyFile';
        $hash = 'hash';

        $fileInfo = $this->createMock(SplFileInfo::class);
        $fileInfo->expects($this->once())->method('getSize')->willReturn(false);
        $fileInfo->expects($this->once())->method('getPathname')->willReturn($path);
        $fileInfo->expects($this->never())->method('getExtension');

        $hasher = $this->createMock(MD5FileHasher::class);
        $hasher->expects($this->never())->method('hash');

        $fileDTOFactory = new FileDTOFactory($hasher);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Can't get size by file $path");
        $fileDTOFactory->buildFromSplFileInfo($fileInfo);
    }
}
