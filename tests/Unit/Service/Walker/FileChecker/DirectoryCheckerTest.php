<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\Walker\FileChecker;

use App\Service\Walker\DTO\PathDTO;
use App\Service\Walker\FileChecker\DirectoryChecker;
use Exception;
use PHPUnit\Framework\TestCase;

class DirectoryCheckerTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testCheck(): void
    {
        $path = __DIR__;
        $pathDTO = new PathDTO($path);
        $directoryChecker = new DirectoryChecker();

        $this->assertTrue($directoryChecker->check($pathDTO));
    }
}
