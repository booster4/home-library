<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\Walker\FileChecker;

use App\Service\Walker\DTO\PathDTO;
use App\Service\Walker\FileChecker\FSChecker;
use Exception;
use PHPUnit\Framework\TestCase;

class FSCheckerTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testCheck(): void
    {
        $path = __FILE__;
        $pathDTO = new PathDTO($path);
        $fsChecker = new FSChecker();

        $this->assertTrue($fsChecker->check($pathDTO));
    }
}
