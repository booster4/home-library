<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\Walker\FileChecker;

use App\Enum\File\FileHashMethod;
use App\Service\Walker\DTO\DirectoryDTO;
use App\Service\Walker\DTO\FileDTO;
use App\Service\Walker\DTO\FileDTOInterface;
use App\Service\Walker\DTO\PathDTO;
use App\Service\Walker\FileChecker\BookChecker;
use Exception;
use Generator;
use PHPUnit\Framework\TestCase;

class BookCheckerTest extends TestCase
{
    /**
     * @dataProvider filePathDataProvider
     * @throws Exception
     */
    public function testCheck(FileDTOInterface $file, bool $result): void
    {
        $bookChecker = new BookChecker();

        $this->assertEquals($result, $bookChecker->check($file));
    }

    public function filePathDataProvider(): Generator
    {
        yield [
            'file' => new FileDTO('path', 'pdf', 'application/pdf', 'hash', FileHashMethod::BODY_MD5, 1024),
            'result' => true,
        ];
        yield [
            'file' => new PathDTO('path'),
            'result' => false,
        ];
        yield [
            'file' => new DirectoryDTO('path'),
            'result' => false,
        ];
        yield [
            'file' => new FileDTO('path', 'txt', 'application/pdf', 'hash', FileHashMethod::BODY_MD5, 1024),
            'result' => false,
        ];
        yield [
            'file' => new FileDTO('path', 'pdf', 'plain/text', 'hash', FileHashMethod::BODY_MD5, 1024),
            'result' => false,
        ];
    }
}
