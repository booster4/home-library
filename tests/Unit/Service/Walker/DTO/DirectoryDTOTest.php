<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\Walker\DTO;

use App\Service\Walker\DTO\DirectoryDTO;
use PHPUnit\Framework\TestCase;

class DirectoryDTOTest extends TestCase
{
    public function testConstructor(): void
    {
        $path = 'path';

        $directoryDTO = new DirectoryDTO($path);
        $this->assertEquals($path, $directoryDTO->getPath());
    }

    public function testGetters(): void
    {
        $directoryDTO = new DirectoryDTO('path');

        $this->assertNull($directoryDTO->getSize());
        $this->assertNull($directoryDTO->getMime());
        $this->assertNull($directoryDTO->getExtension());
        $this->assertNull($directoryDTO->getHashMethod());
        $this->assertTrue($directoryDTO->isDirectory());
        $this->assertNull($directoryDTO->getHash());
    }
}
