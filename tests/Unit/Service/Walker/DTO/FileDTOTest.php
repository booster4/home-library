<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\Walker\DTO;

use App\Enum\File\FileHashMethod;
use App\Service\Walker\DTO\FileDTO;
use PHPUnit\Framework\TestCase;

class FileDTOTest extends TestCase
{
    public function testConstructor(): void
    {
        $path = 'path';
        $extension = 'extension';
        $mime = 'mime';
        $hash = 'hash';
        $hashMethod = FileHashMethod::BODY_MD5;
        $size = 100;

        $fileDTO = new FileDTO($path, $extension, $mime, $hash, $hashMethod, $size);

        $this->assertEquals($path, $fileDTO->getPath());
        $this->assertEquals($extension, $fileDTO->getExtension());
        $this->assertEquals($mime, $fileDTO->getMime());
        $this->assertEquals($hash, $fileDTO->getHash());
        $this->assertEquals($hashMethod, $fileDTO->getHashMethod());
        $this->assertEquals($size, $fileDTO->getSize());
        $this->assertFalse($fileDTO->isDirectory());
    }
}
