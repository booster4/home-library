<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\Walker\DTO;

use App\Service\Walker\DTO\PathDTO;
use PHPUnit\Framework\TestCase;

class PathDTOTest extends TestCase
{
    public function testConstructor(): void
    {
        $path = 'path';

        $pathDTO = new PathDTO($path);
        $this->assertEquals($path, $pathDTO->getPath());
    }

    public function testGetters(): void
    {
        $pathDTO = new PathDTO('path');

        $this->assertNull($pathDTO->getSize());
        $this->assertNull($pathDTO->getMime());
        $this->assertNull($pathDTO->getExtension());
        $this->assertNull($pathDTO->getHashMethod());
        $this->assertNull($pathDTO->isDirectory());
        $this->assertNull($pathDTO->getHash());
    }
}
