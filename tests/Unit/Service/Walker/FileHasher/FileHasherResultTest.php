<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\Walker\FileHasher;

use App\Enum\File\FileHashMethod;
use App\Service\Walker\FileHasher\MD5FileHasherResult;
use PHPUnit\Framework\TestCase;

class FileHasherResultTest extends TestCase
{
    public function testConstructor(): void
    {
        $hash = 'hash';
        $fileHasherResult = new MD5FileHasherResult($hash);

        $this->assertEquals($hash, $fileHasherResult->getHash());
        $this->assertEquals(FileHashMethod::BODY_MD5, $fileHasherResult->getMethod());
    }
}
