<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\Walker\FileHasher;

use App\Service\Walker\FileHasher\MD5FileHasher;
use App\Service\Walker\FileHasher\MD5FileHasherResult;
use PHPUnit\Framework\TestCase;

class FileHasherTest extends TestCase
{
    public function testHash(): void
    {
        $path = __DIR__ . "/FileHasherTest/file.txt";

        $fileHasher = new MD5FileHasher();
        $result = $fileHasher->hash($path);

        $this->assertInstanceOf(MD5FileHasherResult::class, $result);
        $this->assertEquals('f75b8179e4bbe7e2b4a074dcef62de95', $result->getHash());
    }
}
