<?php

namespace App\Tests\Unit\Entity\Default;

use App\Entity\Default\CommandRun;
use App\Enum\CommandRun\Status;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class CommandRunTest extends TestCase
{
    public function testConstructor(): void
    {
        $commandRun = new CommandRun();

        $this->assertEquals(Status::RUN, $commandRun->getStatus());
    }

    public function testGettersAndSetters(): void
    {
        $id = 1;
        $runID = Uuid::v7();
        $name = 'name';
        $status = Status::COMPLETE;
        $runAt = Carbon::now();
        $doneAt = Carbon::now();

        $commandRun = (new CommandRun())
            ->setId($id)
            ->setRunID($runID)
            ->setName($name)
            ->setStatus($status)
            ->setRunAt($runAt)
            ->setDoneAt($doneAt);

        $this->assertEquals($id, $commandRun->getId());
        $this->assertEquals($runID, $commandRun->getRunID());
        $this->assertEquals($name, $commandRun->getName());
        $this->assertEquals($status, $commandRun->getStatus());
        $this->assertEquals($runAt, $commandRun->getRunAt());
        $this->assertEquals($doneAt, $commandRun->getDoneAt());
    }
}
