<?php

declare(strict_types=1);

namespace App\Tests\Unit\Entity\Default;

use App\Entity\Default\Book;
use App\Entity\Default\File;
use App\Entity\Default\File\Path;
use App\Enum\File\FileHashMethod;
use App\Enum\File\Status;
use App\ValueObject\FileSize;
use Carbon\Carbon;
use Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class FileTest extends TestCase
{
    public function testConstructor(): void
    {
        $file = new File();

        $this->assertNull($file->getBook());
        $this->assertNull($file->getUpdatedAt());
        $this->assertEquals(Status::ACTIVE, $file->getStatus());
        $this->assertCount(0, $file->getPaths());
    }

    /**
     * @throws Exception
     */
    public function testGettersAndSetters(): void
    {
        $id = Uuid::v4();
        $hash = md5($id->toRfc4122());
        $hashMethod = FileHashMethod::BODY_MD5;
        $status = Status::DELETE;
        $createdAt = Carbon::now();
        $updatedAt = Carbon::now();
        $size = new FileSize(random_int(0, 100));
        $path1 = new Path();
        $path2 = new Path();
        $mime = 'plain/text';
        $book = new Book();

        $file = (new File())
            ->setID($id)
            ->setHash($hash)
            ->setHashMethod($hashMethod)
            ->setStatus($status)
            ->setCreatedAt($createdAt)
            ->setUpdatedAt($updatedAt)
            ->setSize($size)
            ->setMime($mime)
            ->setBook($book)
            ->addPath($path1)
            ->addPath($path2);

        $this->assertEquals($id, $file->getID());
        $this->assertEquals($hash, $file->getHash());
        $this->assertEquals($hashMethod, $file->getHashMethod());
        $this->assertEquals($status, $file->getStatus());
        $this->assertEquals($createdAt, $file->getCreatedAt());
        $this->assertEquals($updatedAt, $file->getUpdatedAt());
        $this->assertEquals($size, $file->getSize());
        $this->assertEquals($mime, $file->getMime());
        $this->assertEquals($book, $file->getBook());

        $this->assertContains($path1, $file->getPaths());
        $this->assertContains($path2, $file->getPaths());

        $this->assertEquals($file, $path1->getFile());
        $this->assertEquals($file, $path2->getFile());
    }
}
