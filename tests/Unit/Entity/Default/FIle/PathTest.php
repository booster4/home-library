<?php

declare(strict_types=1);

namespace App\Tests\Unit\Entity\Default\FIle;

use App\Entity\Default\File;
use App\Entity\Default\File\Path;
use App\Enum\File\Path\Status;
use Carbon\Carbon;
use DateTime;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class PathTest extends TestCase
{
    public function testConstructor(): void
    {
        $path = new Path();

        $this->assertEquals(Status::ACTIVE, $path->getStatus());
        $this->assertNotNull($path->getCreatedAt());
        $this->assertNull($path->getUpdatedAt());
    }

    public function testGettersAndSetters(): void
    {
        $id = Uuid::v4();
        $pathString = '/var/log/some.log';
        $status = Status::MISSED;
        $createdAt = Carbon::now();
        $updatedAt = Carbon::now();
        $file = new File();

        $path = (new Path())
            ->setID($id)
            ->setPath($pathString)
            ->setStatus($status)
            ->setCreatedAt($createdAt)
            ->setUpdatedAt($updatedAt)
            ->setFile($file);

        $this->assertEquals($id, $path->getID());
        $this->assertEquals($pathString, $path->getPath());
        $this->assertEquals($status, $path->getStatus());
        $this->assertEquals($createdAt, $path->getCreatedAt());
        $this->assertEquals($updatedAt, $path->getUpdatedAt());
        $this->assertEquals($file, $path->getFile());
    }
}
