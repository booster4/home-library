<?php

declare(strict_types=1);

namespace App\Tests\Unit\Entity\Default;

use App\Entity\Default\Author;
use App\Entity\Default\Book;
use App\Entity\Default\File;
use App\Enum\Book\Language;
use App\Enum\Book\Status;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class BookTest extends TestCase
{
    public function testConstructor(): void
    {
        $book = new Book();

        $this->assertEquals(Status::ACTUAL, $book->getStatus());
        $this->assertInstanceOf(ArrayCollection::class, $book->getAuthors());
        $this->assertCount(0, $book->getAuthors());
    }

    public function testGettersAndSetters(): void
    {
        $id = Uuid::v7();
        $title = 'title';
        $status = Status::DELETE;
        $language = Language::ENG;
        $isbn = 'isbn';
        $createdAt = Carbon::now();
        $updatedAt = Carbon::now();
        $author1 = new Author();
        $author2 = new Author();
        $author3 = new Author();

        $file = new File();

        $book = (new Book())
            ->setId($id)
            ->setTitle($title)
            ->setStatus($status)
            ->setLanguage($language)
            ->setFile($file)
            ->setISBN($isbn)
            ->setCreatedAt($createdAt)
            ->setUpdatedAt($updatedAt)
            ->addAuthor($author1)
            ->addAuthor($author2);

        $this->assertEquals($id, $book->getId());
        $this->assertEquals($title, $book->getTitle());
        $this->assertEquals($status, $book->getStatus());
        $this->assertEquals($language, $book->getLanguage());
        $this->assertEquals($file, $book->getFile());
        $this->assertEquals($isbn, $book->getISBN());
        $this->assertEquals($createdAt, $book->getCreatedAt());
        $this->assertEquals($updatedAt, $book->getUpdatedAt());
        $this->assertCount(2, $book->getAuthors());
        $this->assertContains($author1, $book->getAuthors());
        $this->assertContains($author2, $book->getAuthors());

        $book->addAuthor($author1);
        $book->addAuthor($author3);

        $this->assertCount(3, $book->getAuthors());
        $this->assertContains($author3, $book->getAuthors());

        $book->removeAuthor($author3);

        $this->assertCount(2, $book->getAuthors());
        $this->assertNotContains($author3, $book->getAuthors());
    }
}
