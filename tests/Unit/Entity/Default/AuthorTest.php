<?php

declare(strict_types=1);

namespace App\Tests\Unit\Entity\Default;

use App\Entity\Default\Author;
use App\Entity\Default\Book;
use App\Enum\Book\Author\Status;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class AuthorTest extends TestCase
{
    public function testConstructor(): void
    {
        $author = new Author();

        $this->assertEquals(Status::ACTUAL, $author->getStatus());
        $this->assertInstanceOf(ArrayCollection::class, $author->getBooks());
    }

    public function testGettersAndSetters(): void
    {
        $id = Uuid::v7();
        $status = Status::DELETE;
        $createdAt = Carbon::now();
        $updatedAt = Carbon::now();
        $name = 'name';
        $transliteration = 'transliteration';
        $book1 = new Book();
        $book2 = new Book();

        $author = (new Author())
            ->setID($id)
            ->setStatus($status)
            ->setCreatedAt($createdAt)
            ->setUpdatedAt($updatedAt)
            ->setName($name)
            ->setTransliteration($transliteration)
            ->addBook($book1);

        $this->assertEquals($id, $author->getID());
        $this->assertEquals($status, $author->getStatus());
        $this->assertEquals($createdAt, $author->getCreatedAt());
        $this->assertEquals($updatedAt, $author->getUpdatedAt());
        $this->assertEquals($name, $author->getName());
        $this->assertEquals($transliteration, $author->getTransliteration());
        $this->assertCount(1, $author->getBooks());
        $this->assertContains($book1, $author->getBooks());
        $this->assertNotContains($book2, $author->getBooks());

        $author->removeBook($book1);

        $this->assertCount(0, $author->getBooks());
        $this->assertNotContains($book1, $author->getBooks());
    }
}
