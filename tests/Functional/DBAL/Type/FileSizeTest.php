<?php

declare(strict_types=1);

namespace App\Tests\Functional\DBAL\Type;

use App\Entity\Default\File;
use App\Repository\FileRepository;
use App\Service\Walker\DTO\FileDTO;
use App\Tests\Functional\TransactionalTestCase;
use App\Enum\File\FileHashMethod;
use App\ValueObject\FileSize;
use Exception;
use Symfony\Component\Uid\Uuid;

class FileSizeTest extends TransactionalTestCase
{
    /**
     * @throws Exception
     */
    public function testMapping(): void
    {
        $size = 438_575_673;
        $id = Uuid::v7();
        $hash = md5('foo');
        $method = FileHashMethod::BODY_MD5;
        $mime = 'plain/text';

        $file = (new File())
            ->setID($id)
            ->setHash($hash)
            ->setHashMethod($method)
            ->setSize(new FileSize($size))
            ->setMime($mime);

        $this->entityManager->persist($file);
        $this->entityManager->flush();

        /** @var FileRepository $fileRepository */
        $fileRepository = self::getContainer()->get(FileRepository::class);

        $fileDTO = new FileDTO('', '', $mime, $hash, $method, $size);
        $result = $fileRepository->getByHash($fileDTO);

        $this->assertEquals($size, $result->getSize()->getSize());
    }
}
