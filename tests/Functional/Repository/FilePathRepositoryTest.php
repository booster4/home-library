<?php

declare(strict_types=1);

namespace App\Tests\Functional\Repository;

use App\Entity\Default\File;
use App\Entity\Default\File\Path;
use App\Enum\File\FileHashMethod;
use App\Enum\File\Path\Status as PathStatus;
use App\Enum\File\Status as FileStatus;
use App\Repository\Exceptions\FilePathNotFoundException;
use App\Repository\FilePathRepository;
use App\Repository\FilePathRepositoryInterface;
use App\Tests\Functional\TransactionalTestCase;
use App\ValueObject\FileSize;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\Persistence\Mapping\MappingException;
use Exception;
use Symfony\Component\Uid\Uuid;

class FilePathRepositoryTest extends TransactionalTestCase
{
    private FilePathRepositoryInterface $filePathRepository;

    /**
     * @psalm-suppress PropertyTypeCoercion
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->filePathRepository = self::getContainer()->get(FilePathRepository::class);
    }

    /**
     * @throws OptimisticLockException
     * @throws MappingException
     * @throws ORMException
     * @throws FilePathNotFoundException
     */
    public function testGetByID(): void
    {
        $status = PathStatus::ACTIVE;
        $path = 'path';
        $pathID = Uuid::v7();

        $expectedFilePath = (new Path())
            ->setID($pathID)
            ->setStatus($status)
            ->setPath($path);

        $file = (new File())
            ->setID(Uuid::v7())
            ->setMime('mime')
            ->setStatus(FileStatus::ACTIVE)
            ->setSize(new FileSize(100))
            ->addPath($expectedFilePath)
            ->setHash('hash')
            ->setHashMethod(FileHashMethod::BODY_MD5);

        $this->entityManager->persist($file);
        $this->entityManager->flush();
        $this->entityManager->clear();

        $actualFilePath = $this->filePathRepository->getByID($pathID);

        $this->assertEquals($pathID, $actualFilePath->getID());
    }

    public function testGetByIDNotFound(): void
    {
        $id = Uuid::v7();

        $this->expectException(FilePathNotFoundException::class);
        $this->filePathRepository->getByID($id);
    }
}
