<?php

declare(strict_types=1);

namespace App\Tests\Functional\Repository;

use App\Entity\Default\CommandRun;
use App\Enum\CommandRun\Status;
use App\Repository\CommandRunRepository;
use App\Repository\CommandRunRepositoryInterface;
use App\Repository\Exceptions\CommandRunNotFoundByRunIDException;
use App\Tests\Functional\TransactionalTestCase;
use Carbon\Carbon;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\Persistence\Mapping\MappingException;
use Exception;
use Symfony\Component\Uid\Uuid;

class CommandRunRepositoryTest extends TransactionalTestCase
{
    private CommandRunRepositoryInterface $commandRunRepository;

    /**
     * @psalm-suppress PropertyTypeCoercion
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->commandRunRepository = self::getContainer()->get(CommandRunRepository::class);
    }

    /**
     * @throws CommandRunNotFoundByRunIDException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws MappingException
     */
    public function testGetByRunID(): void
    {
        $runID = Uuid::v7();
        $name = "name";
        $runAt = Carbon::now();
        $doneAt = Carbon::now();
        $status = Status::RUN;

        $expectedCommandRun = (new CommandRun())
            ->setRunID($runID)
            ->setName($name)
            ->setRunAt($runAt)
            ->setDoneAt($doneAt)
            ->setStatus($status);

        $this->entityManager->persist($expectedCommandRun);
        $this->entityManager->flush();
        $this->entityManager->clear();

        $actualCommandRun = $this->commandRunRepository->getByRunID($runID);

        $this->assertEquals($expectedCommandRun, $actualCommandRun);
    }

    /**
     * @throws CommandRunNotFoundByRunIDException
     */
    public function testGetByRunIDNotFound(): void
    {
        $runID = Uuid::v7();

        $this->expectException(CommandRunNotFoundByRunIDException::class);
        $this->commandRunRepository->getByRunID($runID);
    }
}
