<?php

declare(strict_types=1);

namespace App\Tests\Functional\Repository;

use App\Entity\Default\File;
use App\Enum\File\FileHashMethod;
use App\Repository\Exceptions\FileNotFoundException;
use App\Repository\FileRepository;
use App\Repository\FileRepositoryInterface;
use App\Service\Walker\DTO\FileDTO;
use App\Tests\Functional\TransactionalTestCase;
use App\ValueObject\FileSize;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\Persistence\Mapping\MappingException;
use Exception;
use Symfony\Component\Uid\Uuid;

class FileRepositoryTest extends TransactionalTestCase
{
    private FileRepositoryInterface $fileRepository;

    /**
     * @psalm-suppress PropertyTypeCoercion
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->fileRepository = self::getContainer()->get(FileRepository::class);
    }

    /**
     * @throws OptimisticLockException
     * @throws MappingException
     * @throws ORMException
     * @throws Exception
     */
    public function testGetByHash(): void
    {
        $hash = 'hash';
        $hashMethod = FileHashMethod::BODY_MD5;
        $id = Uuid::v7();

        $expectedFile = (new File())
            ->setID($id)
            ->setHash($hash)
            ->setMime('mime')
            ->setHashMethod($hashMethod)
            ->setSize(new FileSize(100));

        $this->entityManager->persist($expectedFile);
        $this->entityManager->flush();
        $this->entityManager->clear();

        $fileDTO = new FileDTO('path', 'extension', 'mime', $hash, $hashMethod, 100);

        $file = $this->fileRepository->getByHash($fileDTO);

        $this->assertEquals($id, $file->getID());
    }

    public function testGetByHashNotFound(): void
    {
        $fileDTO = new FileDTO('path', 'extension', 'mime', 'hash', FileHashMethod::BODY_MD5, 100);

        $this->expectException(FileNotFoundException::class);
        $this->fileRepository->getByHash($fileDTO);
    }
}
