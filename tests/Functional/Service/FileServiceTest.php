<?php

declare(strict_types=1);

namespace App\Tests\Functional\Service;

use App\Enum\File\FileHashMethod;
use App\Enum\File\Status;
use App\Repository\Exceptions\FileNotFoundException;
use App\Repository\FileRepositoryInterface;
use App\Service\FileService;
use App\Service\Walker\DTO\FileDTO;
use App\Tests\Functional\TransactionalTestCase;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Exception;

class FileServiceTest extends TransactionalTestCase
{
    private FileService $fileService;

    private FileRepositoryInterface $fileRepository;

    /**
     * @psalm-suppress PropertyTypeCoercion
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->fileService = self::getContainer()->get(FileService::class);
        $this->fileRepository = self::getContainer()->get(FileRepositoryInterface::class);
    }

    /**
     * @throws Exception
     */
    public function testSave(): void
    {
        $fileDTO = new FileDTO('path', 'extension', 'mime', 'hash', FileHashMethod::BODY_MD5, 100);

        $this->fileService->save($fileDTO);
        $this->entityManager->flush();

        $file = $this->fileRepository->getByHash($fileDTO);

        $this->assertEquals($fileDTO->getMime(), $file->getMime());
        $this->assertEquals($fileDTO->getSize(), $file->getSize()->getSize());
        $this->assertEquals($fileDTO->getHash(), $file->getHash());
        $this->assertEquals($fileDTO->getHashMethod(), $file->getHashMethod());
        $this->assertEquals($fileDTO->getPath(), $file->getPaths()->get(0)->getPath());
        $this->assertEquals(Status::ACTIVE, $file->getStatus());
    }

    /**
     * @throws OptimisticLockException
     * @throws FileNotFoundException
     * @throws ORMException
     */
    public function testSaveExistsPath(): void
    {
        $fileDTO1 = new FileDTO('path1', 'extension', 'mime', 'hash', FileHashMethod::BODY_MD5, 100);
        $fileDTO2 = new FileDTO('path2', 'extension', 'mime', 'hash', FileHashMethod::BODY_MD5, 100);
        $fileDTO3 = new FileDTO('path2', 'extension', 'mime', 'hash', FileHashMethod::BODY_MD5, 100);

        $this->fileService->save($fileDTO1);
        $this->entityManager->flush();

        $this->fileService->save($fileDTO2);
        $this->entityManager->flush();

        $this->fileService->save($fileDTO3);
        $this->entityManager->flush();

        $file = $this->fileRepository->getByHash($fileDTO1);

        $this->assertCount(2, $file->getPaths());
        $this->assertEquals($fileDTO1->getPath(), $file->getPaths()->get(0)->getPath());
        $this->assertEquals($fileDTO2->getPath(), $file->getPaths()->get(1)->getPath());
        $this->assertEquals($fileDTO3->getPath(), $file->getPaths()->get(1)->getPath());
        $this->assertEquals(Status::ACTIVE, $file->getStatus());
    }
}
