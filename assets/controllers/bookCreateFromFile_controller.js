import {Controller} from '@hotwired/stimulus';
import $ from 'jquery'

export default class extends Controller {

    connect() {
        const $controller = $('[data-controller]');
        $controller.on('click', '.add-author', function (e) {
            e.preventDefault();

            let $this = $(e.currentTarget);
            let $row = $this.parents('.row').first();
            let $clone = $row.clone();

            $clone.find('input').val('');

            $row.after($clone);
        });

        $controller.on('click', '.remove-author', function (e) {
            e.preventDefault();

            let $this = $(e.currentTarget);
            let $row = $this.parents('.row').first();

            $row.remove();
        });
    }
}
